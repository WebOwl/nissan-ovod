<?php
$models = [];
$cars = array(
    array(
        'name' => 'Qashqai',
        'dir' => 'Qashqai_new',
        'new' => true,
        'order' => 0,
        'image' => 'default.png',
        'model' => 'QASHQAI MC'
    ),
    array(
        'name' => 'X-Trail',
        'dir' => 'X-trail_new',
        'new' => true,
        'order' => 2,
        'image' => 'default.png',
        'model' => 'X-TRAIL'
    ),
    //array(
        //'name' => 'Qashqai',
        //'dir' => 'Qashqai',
        //'order' => 3,
        //'image' => 'default.png',
        //'model' => 'QASHQAI'
    //),
    //array(
        //'name' => 'Juke',
        //'dir' => 'Juke',
        //'order' => 4,
        //'image' => 'default.png',
        //'model' => 'JUKE'
    //),
    /* array(
        'name' => 'Terrano',
        'dir' => 'Terrano',
        'order' => 5,
        'image' => 'default.png',
        'model' => 'TERRANO'
    ),
    array(
        'name' => 'Murano',
        'dir' => 'Murano',
        'new' => true,
        'order' => 6,
        'image' => 'default.png',
        'model' => 'MURANO'
    ), */
    //array(
        //'name' => 'Для корпоративных клиентов',
        //'dir' => 'Corporate',
        //'new' => true,
        //'order' => 6,
        //'image' => 'default.png',
        //'model' => 'Corporate'
    //),
);

$corp_cars = array(
    array(
      'name' => 'Для корпоративных клиентов',
      'dir' => 'Corporate',
      'new' => true,
      'order' => 6,
      'image' => 'default.png',
      'model' => 'Corporate'
    ),
  );


foreach ($cars as $car) {
    $models[] = $car['model'];
}

 // $models[] = 'QASHQAI MC';

$models = array_unique($models);
