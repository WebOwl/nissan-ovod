<?php

$fp = fopen(__DIR__ . '/stock.json', 'r');
$stock = (array)json_decode(fread($fp, filesize(__DIR__ . '/stock.json')));
fclose($fp);

foreach ($stock as &$car) {
    $car = (array)$car;

    unset($car);
}

$priority = array_column($stock, 'priority');
array_multisort($stock, SORT_DESC, $priority);

foreach ($stock as &$car) {
    $color = substr($car['color'], 0, 3);
    foreach ($models as $model) {
        $model = explode(' ', $model);
        $model = $model[0];
        if (strpos($car['model'], $model) !== false)
//            $car['image'] = $model . '/' . $color . '/default.jpg';
            $url = str_replace(" MC", "", $model);
        $url = str_replace (" ","_",$url);
        $car['image'] = $url . '/' . $color . '/default.jpg';
    }
}

foreach ($cars as &$car) {

    $car['count'] = count(
        array_filter($stock,
            function ($item) use ($car) {
                return ((strpos($item['model'], $car['model']) !== false) &&
//                    (strpos(substr($item['model'], strlen($car['model']), strlen($item['model'])), 'NEW') === false) &&
                    (strpos(substr($item['model'], strlen($car['model']), strlen($item['model'])), 'MC') === false)
                );
            }
        )
    );

    unset($car);
}









