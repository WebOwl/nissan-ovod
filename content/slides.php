<?php

$dateTimeZone = new DateTimeZone("Europe/Moscow");
$dateTime = new DateTime("now", $dateTimeZone);
$timeOffset = $dateTimeZone->getOffset($dateTime);

$date = strtotime('+3 day', time());

//
$d = date('j', $date);
$m = date('m', $date);

$month = array(
    '01' => 'января',
    '02' => 'февраля',
    '03' => 'марта',
    '04' => 'апреля',
    '05' => 'мая',
    '06' => 'июня',
    '07' => 'июля',
    '08' => 'августа',
    '09' => 'сентября',
    '10' => 'октября',
    '11' => 'ноября',
    '12' => 'декабря',
);
$day = $d . ' ' . $month[$m];

$slides = array(
    array(
        'image' => 'banner.jpg',
        'utm' => '',
		'text' => '<div class="column">
                        <div>
                            <p class="title">НЕДЕЛЯ ПРОДАЖ NISSAN В ОВОД</p>
                            <p class="title">Мы зафиксировали цены на все! — 3 опции + КАСКО в подарок.</p>
                            <p class="title">Кредит без первого взноса. От 0,1%</p>
                            <!-- <p class="title">Только до ' . $day .' в ОВОД Ниссан</p> -->
                        </div>
</div>'
    )
);

