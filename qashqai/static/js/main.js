$(document).ready(function() {
   $('.top_banner-wrap').attr('style','margin-top:' + $('header .container').height() + 'px');
    $(window).on('resize', function (){
        $('.top_banner-wrap').attr('style', '');
        $('.top_banner-wrap').attr('style','margin-top:' + $('header .container').height() + 'px');
    })

    $("input[name=phone]").mask("+7(999) 999-99-99", {
        noAutoClear: ''
    });

    $(".js-popup-link").magnificPopup({
        type: "inline",
        midClick: true
    });

    var windowWidth = $(window).width();
    var mapWrapHeight = $('.map').height();

    if(window.location.hash) {
        var hash = $(window.location.hash).offset().top;
        $('html, body').animate({
            scrollTop: hash - 100
        }, 1000);
    }


    ymaps.ready(function() {
        var balloon_bg = './static/img/general/maps_bg_ball.png';
        var balloon_offset = [-160, -150];
        var balloon_size = [350, 179];


        var myMap = new ymaps.Map('map', {
                center: [55.581363, 37.705383],
                zoom: 15,
            }, {
                searchControlProvider: 'yandex#search'
            }),
            myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
                balloonContent: "<div class='balloon_container'><span class='maps_dealer_title'>Автоцентр ОВОД</span><span class='maps_dealer_address'>Москва, 26 км МКАД (внешняя сторона) вл.13</span><span class='maps_dealer_time'>Мы работаем с 09:00 до 21:00</span></div>",
                hintContent: 'Renault'
            }, {
                balloonLayout: "default#imageWithContent",
                balloonImageHref: balloon_bg,
                balloonImageOffset: balloon_offset,
                balloonImageSize: balloon_size,
            });
        myMap.geoObjects.add(myPlacemark);
        myPlacemark.balloon.open();
        myMap.behaviors.disable('scrollZoom');
        myMap.controls.remove('trafficControl').remove('searchControl').remove('typeSelector').remove('geolocationControl').remove('fullscreenControl').remove('rulerControl');
    });

     makeSelect(car_data);
     refreshCarList(car_data);
     $('.filter_form__group select').change(function(){
        refreshCarList(car_data);
     })
});



// send form
$("form").on("submit", function() {

    var form = $(this);

    $(form).validate({
        rules: {
            //name: 'required',
            phone: 'required'
        },
        messages: {
            //name: '',
            phone: ''
        }
    });
    if ($(form).valid()) {
        $(this).find('[type=submit]').prop("disabled", true);
        $.post("/promo/lib/email.php", $(this).serialize(), function() {
            // ckForms.send(formID);
            // dataLayer.push({
            //     'event': 'zayvka'
            // });
            if(typeof dataLayer != 'undefined') {
                if ($(form).attr('name') == 'service') {
                    dataLayer.push({'event': 'zayvka-ser'});
                    console.log('service');
                } else {
                    dataLayer.push({'event': 'zayvka'});
                    console.log('misc');
                }
            }
            if(typeof ckForms != 'undefined') {
                ckForms.send(form[0]);
            }
            $.magnificPopup.instance.close();

            if ($('#sucess').length) {
                $.magnificPopup.open({
                    items: {
                        src: $('#sucess')
                    },
                    type: 'inline'
                });
            }
        });
        $('form').trigger('reset');
        $(this).find('[type=submit]').prop("disabled", false);
    }

    return false;
});




$(window).load(function() {
    function textHeight() {
        var topBannerText = $('.top_banner_text').outerHeight();
        var topTextHeigh = $('.top_banner-wrap img').height();
        $('.top_banner_text').css('margin-top', -(topBannerText / 2));
        $('.top_banner_text-wrap').height(topTextHeigh);

    }
    textHeight();

    $(window).resize(function() {
        textHeight();
    });
});

$(".js-show-service").on( 'click', function() {
    $('.service').toggleClass("show");
} );



// tabs
$(function () {

    $(document).delegate(".js-tabs .tab__item", "click", function(){
        var $tab = $(this);
        var $tabContainer = $tab.parent(".js-tabs");
        var nameContent = $tab.data("name");

        if (nameContent && nameContent.length > 0) {

            var $newActiveTabContent = $tabContainer.find(".tab-content_name-" + nameContent);
            if ($newActiveTabContent) {

                $tabContainer.find(".tab-content_active").removeClass("tab-content_active");
                $newActiveTabContent.addClass("tab-content_active");

                $tabContainer.find(".tab__item_active").removeClass("tab__item_active");
                $tab.addClass("tab__item_active");
            }
        }
    });
});

// tabs2
$(function () {

    $(document).delegate(".js-tabs-compl .tab-compl__item", "click", function(){
        var $tab = $(this);
        var $tabContainer = $tab.parent(".js-tabs-compl");
        var nameContent = $tab.data("name");

        if (nameContent && nameContent.length > 0) {

            var $newActiveTabContent = $tabContainer.find(".tab-compl-content_name-" + nameContent);
            if ($newActiveTabContent) {

                $tabContainer.find(".tab-compl-content_active").removeClass("tab-compl-content_active");
                $newActiveTabContent.addClass("tab-compl-content_active");

                $tabContainer.find(".tab-compl__item_active").removeClass("tab-compl__item_active");
                $tab.addClass("tab-compl__item_active");
            }
        }
    });
});

//accordeon
$(document).ready(function(){

    $('.complectation').click(function(event){
        event.stopPropagation();
        var status = this.getAttribute('data-status') || "hidden";
        if (status === 'hidden') {
            $('.complectation-content').removeClass('active')
            $(this).next().addClass('complectation-content__active');
            this.setAttribute('data-status', "shown");

        } else {
            $(this).next().removeClass('complectation-content__active');
            this.setAttribute('data-status', "hidden")
        }
    });

});


$(function() {
    'use strict';

    function filterData(inputId, dataSelector) {

        var entry = document.querySelector(inputId),
            infoBlock = document.querySelectorAll(dataSelector),
            txtValue;

        if (!entry || !infoBlock) {
            return false
        }

        function getInfo(cells) {
            return [].map.call(cells, function (el) {
                var valueEl = el.getAttribute("data-model");
                return valueEl;
            });
        }

        var infoTxt = getInfo(infoBlock);


        entry.addEventListener('change', function (event) {
            txtValue = this.value;
            if (txtValue === "all") {
                for (var i = 0, len = infoBlock.length; i < len; i++) {
                    infoBlock[i].style.display = '';

                }
			console.log(txtValue);
            } else {
                if (txtValue.length > 1) {

                    (function () {
                        var patt = new RegExp(txtValue, 'i');
                        for (var i = 0, n = infoTxt.length; i < n; i++) {
                            var testResult;
                            testResult = patt.test(infoTxt[i]);

                            //console.log(i + ' - ' + testResult);

                            if (testResult) {
                                infoBlock[i].style.display = '';

                            }
                            else {
                                infoBlock[i].style.display = 'none';
                            }
                        }
                    })();
                }
                else {
                    (function () {
                        for (var i = 0, n = infoTxt.length; i < n; i++) {
                            infoBlock[i].setAttribute('style', '');
                        }
                    })();
                }
            }
        });

    }

    filterData('#seletor', '.js-item-info');

});

function makeSelect(data){
    data_columns = {model: [], year:[], color:[]};
    data.forEach(function(i){
        //console.log(Object.getOwnPropertyNames(i));
        Object.getOwnPropertyNames(i).forEach(function(key){
            //console.log(key + ' -> ' + i[key]);
            if(typeof(data_columns[key]) != 'undefined'){
                if(!data_columns[key].includes(i[key])){
                    data_columns[key].push(i[key]);       
                }                 
            }            
        })
    })
    makeOptions('[name="form_model"]', data_columns.model);
    makeOptions('[name="form_color"]', data_columns.color);
    makeOptions('[name="form_year"]', data_columns.year);
    console.log(data_columns);
}

function makeOptions(selector, data){
    data.forEach(function(i){
        //console.log(i);
        $(selector).append('<option value="'+i+'">'+i+'</option>')
    })
}

function refreshCarList(data){
    var model = $('[name="form_model"]').val();
    var year = $('[name="form_year"]').val();
    var color = $('[name="form_color"]').val();
    var newData = [];
    data.forEach(function(i){
        if(
            ((model == 0)   ||  (model == i.model))  &&
            ((year == 0)    ||  (year == i.year))    &&
            ((color == 0)   ||  (color == i.color))
            ) {
                newData.push(i);
            }
        }
    );
    //console.log(newData);
    $('.filter_section .car_list').html(""); //зачищаем список
    newData.forEach(function(i, index, array){
        renderCarItem(i, index);

    })

    //обновляем select
    $('.filter_section select').html(""); //зачищаем список
    $('.filter_section select').append('<option value="0">Все</option>');
    makeSelect(newData);
    $('[name="form_model"]').val(model);
    $('[name="form_year"]').val(year);
    $('[name="form_color"]').val(color);

    //навешиваем формы
    $(".js-popup-link").magnificPopup({
        type: "inline",
        midClick: true
    });

}

function renderCarItem(item, id){
    var car_item_view = car_view;
    car_item_view = car_item_view.replace("%id%", id+1);
    car_item_view = car_item_view.replace("%model%", item.model);
    car_item_view = car_item_view.replace("%img%", getImg(item.year, item.color));
    car_item_view = car_item_view.replace("%color%", item.color);
    car_item_view = car_item_view.replace("%year%", item.year);
    car_item_view = car_item_view.replace("%price_rrc%", item.price_rrc);
    car_item_view = car_item_view.replace("%profit%", item.profit);
    $('.filter_section .car_list').append(car_item_view);
}

function getImg(year, color){
    return year+'/'+color.replace(' /G','')+'.jpg';
}