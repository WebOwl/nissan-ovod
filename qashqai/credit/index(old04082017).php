<?php

    /* @var $complectations Complectation[] */
    require_once __DIR__ .'/../../lib/complectations.php';
    $currentModel = 'qashqai';
    $complectations = Complectation::getFromExcel($currentModel);

    $monthRU = array('01' => 'января', '02' => 'февраля', '03' => 'марта', '04' => 'апреля', '05' => 'мая', '06' => 'июня', '07' => 'июля', '08' => 'августа', '09' => 'сентября', '10' => 'октября', '11' => 'ноября', '12' => 'декабря');
    $nextDay = date('j', strtotime(' +5 day'));
    $nextMonth = strtoupper($monthRU[date('m', strtotime(' +5 day'))]);
?>
<title>Купить Nissan Qashqai у официального дилера ОВОД</title>
<!DOCTYPE html>
<html lang="ru-RU">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE = edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="keywords" content="">
    <link rel="stylesheet" type="text/css" href="../static/fonts/fonts.css">
    <link rel="stylesheet" type="text/css" href="../static/css/main.css">
    <title>Купить Nissan Qashqai у официального дилера ОВОД</title>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
      new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
      j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
      'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5GM8W2');</script>
    <!-- End Google Tag Manager -->
  </head>
  <body>
  <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5GM8W2"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <div class="wrapper">
      <header>
        <div class="container">
          <div class="header">
            <div class="header_dealer-logo"><img src="../static/img/general/dealer_logo.png" alt=""></div>
            <div class="header_addres"><a href="#map"><span class="street">Москва, 26 км МКАД (внешняя сторона), вл. 13</span></a><span class="phone">+7 (495) 787-53-53</span><a class="js-popup-link btn btn-red" href="#feedback">Обратный звонок</a></div>
            <div class="header_brand-logo"><img src="../static/img/general/brand_logo.jpg" alt=""></div>
          </div>
        </div>
      </header>
      <div class="wrapper">
        <section class="top_banner-wrap"><img class="top_banner_big" src="../static/img/banner/qashqai.png" alt="">
          <div class="top-banner__text" style="left: 56%;">
            <div class="top-banner__date"><span>Только до<br> <?= $nextDay ?> <?= $nextMonth ?></span></div>
        <div class="top-banner__benefit"><span class="banner-benefits__left-text">Успей получить<br> кредит на <br>Nissan QASHQAI</span><span class="banner-benefits__right-text" style="font-size: 30px"><span class="benefits-price">8 500 </span> ₽/мес<sup>1</sup></span></div>
          </div>
        </section><a class="scroll_link" id="spec"></a>
        <section class="promo_title-wrap gray_bg">
          <div class="container">
            <div class="promo_title"><span class="promo_title_top-text"><span class="white_text">ТОЛЬКО ДО</span> <?= $nextDay ?> <?= $nextMonth ?><br> <span class="white_text">последние автомобили NISSAN Qashqai <br> ПО ЦЕНАМ НИЖЕ РЫНКА!</span></span>
        <span class="main-benef__item"><span class="red_text">✔</span> Кредит от 8500 ₽/мес<sup>1</sup></span><span class="main-benef__item"><span class="red_text">✔</span> Кредит от 8% на 3 года<sup>1</sup></span><span class="main-benef__item"><span class="red_text">✔</span> В наличии с ПТС<sup>2</sup></span><span class="main-benef__item"><span class="red_text">✔</span> Выгодный Trade-in</span>
              <form class="top_form" id="f-promo_title" action="" data-callkeeper_name="Получить лучшую цену!">
                <input type="text" name="name" placeholder="Имя" data-callkeeper="person">
                <input type="text" name="phone" placeholder="Телефон" data-callkeeper="tel">
                <div class="submit_wrap">
                  <input class="btn btn-red" type="submit" value="Получить выгоду">
                </div>
              </form>
            </div>
          </div>
        </section><span class="section_title">Nissan QASHQAI по специальной цене</span>
        <div class="complect" id="complect_sentra" style="display: block;"><a name="Complect_list-sentra"></a>
          <div class="complect_left_right">
            <div class="complect_left complect_block"><span class="model_title">Комплектации Nissan QASHQAI
                <div class="complect_img">
                  <div class="tab js-tabs">
                    <div class="tab-content tab-content_name-tab-1 tab-content_active"><img src="../static/img/content/cars/1.png"></div>
            <div class="tab-content tab-content_name-tab-2"><img src="../static/img/content/cars/2.png"></div>
                    <div class="tab-content tab-content_name-tab-3"><img src="../static/img/content/cars/3.png"></div>
                    <div class="tab-content tab-content_name-tab-4"><img src="../static/img/content/cars/4.png"></div>
                    <div class="tab-content tab-content_name-tab-5"><img src="../static/img/content/cars/5.png"></div>
                    <div class="tab-content tab-content_name-tab-6"><img src="../static/img/content/cars/6.png"></div>
                    <div class="tab-content tab-content_name-tab-7"><img src="../static/img/content/cars/7.png"></div>
                    <div class="tab-content tab-content_name-tab-8"><img src="../static/img/content/cars/8.png"></div>
                    <div class="tab-content tab-content_name-tab-9"><img src="../static/img/content/cars/9.png"></div>
                    <a class="tab__item tab__item_active" data-name="tab-1"></a><a class="tab__item" data-name="tab-2"></a><a class="tab__item" data-name="tab-3"></a><a class="tab__item" data-name="tab-4"></a><a class="tab__item" data-name="tab-5"></a><a class="tab__item" data-name="tab-6"></a><a class="tab__item" data-name="tab-7"><a class="tab__item" data-name="tab-8"><a class="tab__item" data-name="tab-9"></a>
                  </div>
                </div></span></div>

        <div class="complect_right complect_block"><span class="model_price">Цена от 985 000 ₽</span><span class="model_benefit">Кредит от 8 500 ₽/мес <sup>1</sup></span>
              <div class="model_button-yellow">
                <!-- dr start--><a class="js-popup-link" href="#best_price">Получить выгоду</a>
                <!-- dr end-->
              </div>
              <div class="model_button-white"><a class="js-popup-link" href="#test-drive" style="margin-bottom: 20px;">ЗАПИСАТЬСЯ НА ТЕСТ-ДРАЙВ</a></div>
              <div class="model_button-white"><a class="js-popup-link" href="#credit" style="margin-bottom: 20px;">РАССЧИТАТЬ ЛЬГОТНЫЙ КРЕДИТ</a></div>
              <div class="model_button-white"><a class="js-popup-link" href="#change-auto" style="margin-bottom: 20px;">ВЫГОДНО ОБМЕНЯТЬ СВОЙ АВТО НА ЭТОТ</a></div>
              <div class="model_button-white">
                <!-- dr start--><a class="js-popup-link" href="#feedback" style="margin-bottom: 20px;">ЗАКАЗАТЬ ОБРАТНЫЙ ЗВОНОК БЕСПЛАТНО</a>
                <!-- dr end-->
              </div>
            </div>
          </div>


<div class="complect_list">
            <div class="tab-compl js-tabs-compl">
              <form id="filter_table">
                <select id="seletor" name="models">
                  <option value="all">Выберите комплектацию</option>
                    <? foreach (array_unique(Complectation::getFromExcel($currentModel, true)) as $complectation): ?>
                        <option value="<?= $complectation ?>"><?= $complectation ?></option>
                    <? endforeach; ?>
                </select>
              </form>
      <table class="complectation-item">
        <thead>
        <tr>
          <td class="cell-first">Комплектация</td>
          <td class="cell-ather">Цвет</td>
          <td class="cell-ather">Год</td>
          <td class="cell-ather">Цена от</td>
          <td></td>
        </tr>
        </thead>
                <? foreach ($complectations as $complectation): ?>
                    <tr data-model="<?= $complectation->complectation ?>" class="cplectation-tr js-item-info">
                        <td data-lable="Комплектация" class="cell-first"><?= $complectation->model ?> <nobr><?= $complectation->complectation ?></nobr><br>
                            <span class="cell-first-text">Кредит 0%</span><a class="cell-first-link js-popup-link" href="#credit"><nobr>Рассчитать кредит >></nobr></a>
                        </td>
                        <td data-lable="Цвет" class="cell-ather"><?= $complectation->color ?></td>
                        <td data-lable="Год" class="cell-ather"><?= $complectation->year ?></td>
                        <td data-lable="Цена от" class="cell-ather">
                            <? if ($complectation->price): ?>
                                <strike><?= number_format($complectation->price, 0, '', ' ') ?> ₽</strike>
                            <? endif ?>
                        </td>
                        <td data-lable="" class="cell-ather"><button class="js-popup-link" href="#best_price">Получить выгоду</button></td>
                    </tr>
                <? endforeach; ?>
      </table>



            </div>
          </div>
        </div><a class="scroll_link" id="models"></a>
        <section class="car_block-wrap">
          <h2>Автомобили по специальным ценам</h2>
          <div class="car-block">
                <div class="car-block__container">
                    <div class="car_item-wrap">
                        <div class="car_item" style="min-height:400px;"><span class="car_title">Almera</span>
                            <div class="car_img_wrap"><img src="../static/img/content/cars/almera.png" alt=""></div>
                            <div class="car_item-text"><span class="car_price">от 491 000 ₽</span><span class="car_beneff red_text" style="display: block !important;">Выгода до 150 000 ₽<sup>4</sup></span><span class="car_beneff red_text" style="display: block !important;">Кредит 3% на 3 года<sup>1</sup></span></div>
                            <div class="car_item-buttons"><a class="js-popup-link btn btn-red" href="#best_price">Получить лучшую цену</a><a class="js-popup-link btn btn-black" href="#credit">Купить в кредит<sup></sup></a></div>
                        </div>
                    </div>
                    <div class="car_item-wrap">
                        <div class="car_item" style="min-height:400px;"><span class="car_title">Terrano</span>
                            <div class="car_img_wrap"><img src="../static/img/content/cars/terrano.png" alt=""></div>
                            <div class="car_item-text"><span class="car_price">от 795 000 ₽</span><span class="car_beneff red_text" style="display:block !important;">Выгода до 70 000 ₽<sup>4</sup></span><span class="car_beneff red_text" style="display:block !important;">Кредит от 6 000 ₽/мес<sup>1</sup></span></div>
                            <div class="car_item-buttons"><a class="js-popup-link btn btn-red" href="#best_price">Получить лучшую цену</a><a class="js-popup-link btn btn-black" href="#credit">Купить в кредит<sup></sup></a></div>
                        </div>
                    </div>
                    <div class="car_item-wrap">
                        <div class="car_item" style="min-height:400px;"><span class="car_title">Qashqai</span>
                            <div class="car_img_wrap"><img src="../static/img/content/cars/qashqai.png" alt=""></div>
                            <div class="car_item-text"><span class="car_price">от 985 000 ₽</span><span class="car_beneff red_text" style="display: block !important;"> Кредит от 8500 ₽/мес<sup>1</sup></span><!--<span class="car_beneff red_text" style="display: block !important;">Кредит 8% на 3 года<sup>1</sup></span> --></div>
                            <div class="car_item-buttons"><a class="js-popup-link btn btn-red" href="#best_price">Получить лучшую цену</a><a class="js-popup-link btn btn-black" href="#credit">Купить в кредит<sup></sup></a></div>
                        </div>
                    </div>
                    <div class="car_item-wrap">
                        <div class="car_item" style="min-height:400px;"><span class="car_title">X-Trail</span>
                            <div class="car_img_wrap"><img src="../static/img/content/cars/xtrail.png" alt=""></div>
                            <div class="car_item-text"><span class="car_price">от 1 264 000 ₽</span><!--<span class="car_beneff red_text" style="display: block !important;">Выгода до 170 000 ₽<sup>4</sup></span>--><span class="car_beneff red_text" style="display: block !important;">Кредит 8% на 3 года <sup>1</sup></span><span class="car_beneff red_text" style="display:block !important;">Кредит от 10 500 ₽/мес<sup>1</sup></span></div>
                            <div class="car_item-buttons"><a class="js-popup-link btn btn-red" href="#best_price">Получить лучшую цену</a><a class="js-popup-link btn btn-black" href="#credit">Купить в кредит<sup></sup></a></div>
                        </div>
                    </div>
                    <div class="car_item-wrap">
                        <div class="car_item" style="min-height:400px;"><span class="car_title">Sentra</span>
                            <div class="car_img_wrap"><img src="../static/img/content/cars/sentra.png" alt=""></div>
                            <div class="car_item-text"><span class="car_price">от 896 000 ₽</span>
                                <!-- <span class="car_beneff red_text"">Выгода 176 000 руб.</span>--><span class="car_beneff red_text" style="display: block !important;">Кредит 0% на 3 года<sup>1</sup></span>
                            </div>
                            <div class="car_item-buttons"><a class="js-popup-link btn btn-red" href="#best_price">Получить лучшую цену</a><a class="js-popup-link btn btn-black" href="#credit">Купить в кредит<sup></sup></a></div>
                        </div>
                    </div>
                    <div class="car_item-wrap">
                        <div class="car_item" style="min-height:400px;"><span class="car_title">Murano</span>
                            <div class="car_img_wrap"><img src="../static/img/content/cars/murano.png" alt=""></div>
                            <div class="car_item-text"><span class="car_price">от 2 460 000 ₽</span><span class="car_beneff red_text" style="display:block !important;">Кредит от 18 000 ₽/мес<sup>1</sup></span><span class="car_beneff red_text" style="display: block !important;">Выгода до 100 000 ₽<sup>4</sup></span><span class="car_beneff red_text" style="display: block !important;">КАСКО в подарок<sup>3</sup></span></div>
                            <div class="car_item-buttons"><a class="js-popup-link btn btn-red" href="#best_price">Получить лучшую цену</a><a class="js-popup-link btn btn-black" href="#credit">Купить в кредит<sup></sup></a></div>
                        </div>
                    </div>
                    <div class="car_item-wrap">
                        <div class="car_item" style="min-height:400px;"><span class="car_title">Pathfinder</span>
                            <div class="car_img_wrap"><img src="../static/img/content/cars/pathfinder.png" alt=""></div>
                            <div class="car_item-text"><span class="car_price">от 2 375 000 ₽</span><span class="car_beneff red_text" style="display: block !important;">Выгода до 380 000 ₽<sup>4</sup></span><span class="car_beneff red_text" style="display: block !important;">Кредит 12% на 3 года<sup>1</sup></span></div>
                            <div class="car_item-buttons"><a class="js-popup-link btn btn-red" href="#best_price">Получить лучшую цену</a><a class="js-popup-link btn btn-black" href="#credit">Купить в кредит<sup></sup></a></div>
                        </div>
                    </div>
                    <div class="car_item-wrap">
                        <div class="car_item" style="min-height:400px;"><span class="car_title">Patrol</span>
                            <div class="car_img_wrap"><img src="../static/img/content/cars/patrol.png" alt=""></div>
                            <div class="car_item-text"><span class="car_price">от 3 965 000 ₽</span><span class="car_beneff red_text" style="display: block !important;">Выгода до 585 000 ₽<sup>4</sup></span><span class="car_beneff red_text" style="display: block !important;">Кредит 12% на 3 года<sup>1</sup></span></div>
                            <div class="car_item-buttons"><a class="js-popup-link btn btn-red" href="#best_price">Получить лучшую цену</a><a class="js-popup-link btn btn-black" href="#credit">Купить в кредит<sup></sup></a></div>
                        </div>
                    </div>
                </div>
            </div>

        </section><a class="scroll_link" id="preim"></a>
        <section class="our_advantages-wrap gray_bg">
          <div class="our-advantages">
            <h2>Наши преимущества</h2>
            <ul class="advantages_list">
              <li style="    padding-top: 23px;">
                <p style="text-align: center; vertical-align: middle;"><span>Официальный дилер<br> с 2000 года</span></p>
              </li>
        <li><span class="inline">от</span><span class="advantages_title inline">&nbsp;0%<sup></sup></span><span>Кредит<sup>1</sup></span></li>
              <li><span class="advantages_title">КАСКО<sup></sup></span><span>в подарок!<sup>3</sup></span></li>
              <li><span class="advantages_title">ПТС</span><span>Авто в наличии с ПТС<sup>2</sup></span></li>
              <li><span class="advantages_title">Trade-in</span><span>Выгодный обмен авто на новый</span></li>
              <li class="only_text"><span>Гарантия 3 года или 100 000 км</span></li>
            </ul>
          </div>
        </section><a class="scroll_link" id="contacts"></a>
        <section class="bottom_open_form-wrap">
          <div class="container">
            <div class="bottom_open_form">
              <h2>Получите лучшее предложение</h2>
              <form class="bottom_form" id="f-bottom_open_form" action="">
                <input type="text" name="name" placeholder="Ваше имя" data-callkeeper="person">
                <input type="text" name="phone" placeholder="Ваш телефон" data-callkeeper="tel">
                <div class="submit_wrap">
                  <input class="btn btn-red" type="submit" value="Отправить">
                </div>
              </form>
            </div>
          </div>
          <div id="map"></div>
        </section>

      <section class="disclaimer-wrap">
    <p>1. Маркетинговая ставка не является процентной ставкой по кредиту и отражает размер расходов по уплате процентов на сумму кредита, предоставляемого АО «РН Банк» (лицензия Банка России № 170 от 16.12.2014 г.) для приобретения автомобиля Nissan Sentra все комплектации МТ 2016 г. в. стоимостью до 1 150 000 руб., достигаемый соразмерным снижением стоимости приобретаемого автомобиля. Предложение не является офертой, действительно с 01.06.2017 по 30.06.2017 для всех клиентов при соблюдении следующих условий: предоставление приобретаемого автомобиля в залог АО «РН Банк»; первоначальный взнос: от 50% стоимости автомобиля; сумма кредита: от 100 тыс. руб.; валюта кредита: рубль РФ; срок кредита: 3 года; процентная ставка: 9,4% годовых; ежемесячные равные (аннуитетные) платежи по погашению основной суммы долга и уплате процентов по кредитному договору; оплата страховой премии по следующим договорам: по договору страхования от несчастных случаев и болезней по программе «Защищенный кредит» с ООО «СК КАРДИФ» (лицензия Банка России СЛ № 4104 от 06.11.2015 г.) и договору КАСКО по любой программе Nissan Страхование сроком на 1 год со СПАО «Ингосстрах» (лицензия Банка России СИ № 0928 от 23.09.2015 г.). Условия и тарифы могут быть изменены в одностороннем порядке. Подробности – на www.nissan.ru. Размер ежемесячных равных (аннуитетных) платежей, кроме последнего в размере 376000/704000/1096000 рублей1 по погашению основной суммы долга и уплате процентов по кредитному договору рассчитан из следующих условий: приобретение нового автомобиля по рекомендованной розничной цене с учетом скидки 905707/1689763/2630654 рублей1 в кредит от АО «РН Банк» (лицензия Банка России № 170 (бессрочная)) и его предоставление в залог АО «РН Банк»; первоначальный взнос 488800/968000/1507000 рублей1; общая сумма кредита: 440705/762963/1187795 рублей1; валюта кредита: рубль РФ; срок кредита: 36 мес.; процентная ставка: для Nissan Terrano-11,25% годовых, для Nissan Qashqai, Murano – 17% годовых; оплата не в кредит страховой премии по договору КАСКО со страховой компанией, соответствующей требованиям АО «РН Банк», а также оплата с использованием кредита страховой премии по договору страхования от несчастных случаев и болезней по программе «Защищенный кредит» сроком на 3 года в сумме 23798/41200/64141 рублей1 с ООО «СК КАРДИФ» (лицензия Банка России СЛ № 4104 от 06.11.2015 г.). Условия и тарифы могут быть изменены в одностороннем порядке. Предложение не является офертой, действительно с 01.06.2017 по 30.06.2017 года. Подробности на www.nissan.ru</p>
            <p>2. Полный список автомобилей уточняйте в отделе продаж. Не является офертой.
            </p>
            <p>3. Сумма страховой премии, оплачиваемой клиентом в пользу ПАО СК «Росгосстрах» (лицензия Банка России на осуществление страхования СИ № 0001 от 23.05.2016 г., бессрочная) или СПАО «Ингосстрах» (лицензия Банка России на осуществление страхования СИ № 0928 от 23.09.2015 г.) при заключении клиентом договора страхования «Полное КАСКО» в рамках программы «Nissan Страхование», компенсируется соразмерным снижением цены на приобретаемый автомобиль Nissan Murano 2016 г.в. Предложение не является офертой, действительно до 30.06.2017 г. для клиентов в возрасте от 35 лет с водительским стажем от 10 лет. Подробности на www.nissan.ru и по телефону горячей линии 8-800-200-59-90 (звонок по РФ бесплатный).
            </p>
            <p>4. Максимальная выгода 585 000 руб. предоставляется на автомобили Nissan Patrol 2015 г. в. и складывается из выгоды в размере 550 000 руб., а также выгоды в размере 35 000 руб. для клиентов-участников программы «В кругу Nissan» (подробности на www.nissan.ru/RU/ru/vkrugunissan) при сдаче предыдущего автомобиля Nissan по схеме «трейд-ин». Предложение ограничено и действует с 01.06.2017 по 30.06.2017. Количество автомобилей ограничено. О наличии автомобилей уточняйте в дилерских центрах.</p>
            <br>
            <p>Автомобили на имидже могут отличаться от участвующих в акции авто.</p>
    
      </section>

        
      </div>
      <div class="popup-form mfp-hide" id="feedback"><span class="popup-form__title">ЕСТЬ ВОПРОСЫ? ОСТАВЬТЕ ТЕЛЕФОН!</span>
        <form id="f-feedback" action="" data-callkeeper_name="Заказать обратный звонок!">
          <input type="text" name="name" placeholder="Ваше имя" data-callkeeper="person">
          <input type="text" name="phone" placeholder="Ваш телефон" data-callkeeper="tel">
          <input class="btn btn-red" type="submit" value="ПЕРЕЗВОНИТЕ МНЕ">
      <span class="form-discl">Нажав кнопку «Отправить», я даю согласие на обработку <a class="under" target="_blank" href="http://ovod-nissan.ru/about/about-us/politika-v-otnoshenii-obrabotki-personalnyh-dannyh-avtocentra-ovod.php"> моих персональных данных и получение рекламы. </a></span>
        </form>
      </div>
      <div class="popup-form mfp-hide" id="best_price"><span class="popup-form__title">Желаете выгоду больше?<br>Закажите обратный звонок!</span>
        <form id="f-best_price" action="" data-callkeeper_name="Получить лучшую цену!">
          <input type="text" name="name" placeholder="Ваше имя" data-callkeeper="person">
          <input type="text" name="phone" placeholder="Ваш телефон" data-callkeeper="tel">
          <input class="btn btn-red" type="submit" value="ПОЛУЧИТЬ ВЫГОДУ">
      <span class="form-discl">Нажав кнопку «Отправить», я даю согласие на обработку <a class="under" target="_blank" href="http://ovod-nissan.ru/about/about-us/politika-v-otnoshenii-obrabotki-personalnyh-dannyh-avtocentra-ovod.php"> моих персональных данных и получение рекламы. </a></span>
        </form>
      </div>
      <div class="popup-form mfp-hide" id="test-drive"><span class="popup-form__title">Хотите пройти тест-драйв на этом автомобиле?</span>
        <form id="f-best_price" action="" data-callkeeper_name="Получить лучшую цену!">
          <input type="text" name="name" placeholder="Ваше имя" data-callkeeper="person">
          <input type="text" name="phone" placeholder="Ваш телефон" data-callkeeper="tel">
          <input class="btn btn-red" type="submit" value="ЗАПИСАТЬСЯ НА ТЕСТ-ДРАЙВ">
      <span class="form-discl">Нажав кнопку «Отправить», я даю согласие на обработку <a class="under" target="_blank" href="http://ovod-nissan.ru/about/about-us/politika-v-otnoshenii-obrabotki-personalnyh-dannyh-avtocentra-ovod.php"> моих персональных данных и получение рекламы. </a></span>
        </form>
      </div>
      <div class="popup-form mfp-hide" id="credit"><span class="popup-form__title">Интересует покупка в кредит? Получите выгодное кредитное предложение прямо сейчас!</span>
        <form id="f-credit" action="" data-callkeeper_name="Заявка на кредит!">
          <input type="text" name="name" placeholder="Ваше имя" data-callkeeper="person">
          <input type="text" name="phone" placeholder="Ваш телефон" data-callkeeper="tel">
          <input class="btn btn-red" type="submit" value="ПОЛУЧИТЬ ИНФОРМАЦИЮ">
      <span class="form-discl">Нажав кнопку «Отправить», я даю согласие на обработку <a class="under" target="_blank" href="http://ovod-nissan.ru/about/about-us/politika-v-otnoshenii-obrabotki-personalnyh-dannyh-avtocentra-ovod.php"> моих персональных данных и получение рекламы. </a></span>
        </form>
      </div>
      <div class="popup-form mfp-hide" id="change-auto"><span class="popup-form__title">УЗНАЙТЕ, КАК ВЫГОДНО ОБМЕНЯТЬ СВОЙ АВТО НА НОВЫЙ NISSAN</span>
        <form id="f-insurance" action="" data-callkeeper_name="Получить КАСКО!">
          <input type="text" name="name" placeholder="Ваше имя" data-callkeeper="person">
          <input type="text" name="phone" placeholder="Ваш телефон" data-callkeeper="tel">
          <input class="btn btn-red" type="submit" value="ПОЛУЧИТЬ ИНФОРМАЦИЮ">
      <span class="form-discl">Нажав кнопку «Отправить», я даю согласие на обработку <a class="under" target="_blank" href="http://ovod-nissan.ru/about/about-us/politika-v-otnoshenii-obrabotki-personalnyh-dannyh-avtocentra-ovod.php"> моих персональных данных и получение рекламы. </a></span>
        </form>
      </div>
      <div class="popup-form mfp-hide" id="sucess"><span class="popup-form__title">Спасибо. Ваша заявка отправлена</span></div>
    </div>
  <script src="../static/js/jquery.js"></script>
    <script src="../static/js/libs.min.js"></script>
    <script src="js/main.js"></script>
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU"></script>
  </body>
</html>