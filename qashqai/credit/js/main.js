$(document).ready(function() {
    $("input[name=phone]").mask("+7(999) 999-99-99", {
        noAutoClear: ''
    });

    $(".js-popup-link").magnificPopup({
        type: "inline",
        midClick: true
    });

    var windowWidth = $(window).width();
    var mapWrapHeight = $('.map').height();

    if(window.location.hash) {
        var hash = $(window.location.hash).offset().top;
        $('html, body').animate({
            scrollTop: hash - 100
        }, 1000);
    }


    ymaps.ready(function() {
        var balloon_bg = '../static/img/general/maps_bg_ball.png';
        var balloon_offset = [-160, -150];
        var balloon_size = [350, 179];


        var myMap = new ymaps.Map('map', {
                center: [55.581363, 37.705383],
                zoom: 15,
            }, {
                searchControlProvider: 'yandex#search'
            }),
            myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
                balloonContent: "<div class='balloon_container'><span class='maps_dealer_title'>Автоцентр ОВОД</span><span class='maps_dealer_address'>Москва, 26 км МКАД (внешняя сторона) вл.13</span><span class='maps_dealer_time'>Мы работаем с 09:00 до 21:00</span></div>",
                hintContent: 'Renault'
            }, {
                balloonLayout: "default#imageWithContent",
                balloonImageHref: balloon_bg,
                balloonImageOffset: balloon_offset,
                balloonImageSize: balloon_size,
            });
        myMap.geoObjects.add(myPlacemark);
        myPlacemark.balloon.open();
        myMap.behaviors.disable('scrollZoom');
        myMap.controls.remove('trafficControl').remove('searchControl').remove('typeSelector').remove('geolocationControl').remove('fullscreenControl').remove('rulerControl');
    });
});



// send form
$("form").on("submit", function() {

    var form = $(this);

    $(form).validate({
        rules: {
            name: 'required',
            phone: 'required'
        },
        messages: {
            name: '',
            phone: ''
        }
    });
    if ($(form).valid()) {
        $(this).find('[type=submit]').prop("disabled", true);
        $.post("/promo/lib/email.php", $(this).serialize(), function() {
            // ckForms.send(formID);
            // dataLayer.push({
            //     'event': 'zayvka'
            // });
            if(typeof dataLayer != 'undefined') {
                if ($(form).attr('name') == 'service') {
                    dataLayer.push({'event': 'zayvka-ser'});
                    console.log('service');
                } else {
                    dataLayer.push({'event': 'zayvka'});
                    console.log('misc');
                }
            }
            if(typeof ckForms != 'undefined') {
                ckForms.send(form[0]);
            }
            $.magnificPopup.instance.close();

            if ($('#sucess').length) {
                $.magnificPopup.open({
                    items: {
                        src: $('#sucess')
                    },
                    type: 'inline'
                });
            }
        });
        $('form').trigger('reset');
        $(this).find('[type=submit]').prop("disabled", false);
    }

    return false;
});




$(window).load(function() {
    function textHeight() {
        var topBannerText = $('.top_banner_text').outerHeight();
        var topTextHeigh = $('.top_banner-wrap img').height();
        $('.top_banner_text').css('margin-top', -(topBannerText / 2));
        $('.top_banner_text-wrap').height(topTextHeigh);

    }
    textHeight();

    $(window).resize(function() {
        textHeight();
    });
});

$(".js-show-service").on( 'click', function() {
    $('.service').toggleClass("show");
} );



// tabs
$(function () {

    $(document).delegate(".js-tabs .tab__item", "click", function(){
        var $tab = $(this);
        var $tabContainer = $tab.parent(".js-tabs");
        var nameContent = $tab.data("name");

        if (nameContent && nameContent.length > 0) {

            var $newActiveTabContent = $tabContainer.find(".tab-content_name-" + nameContent);
            if ($newActiveTabContent) {

                $tabContainer.find(".tab-content_active").removeClass("tab-content_active");
                $newActiveTabContent.addClass("tab-content_active");

                $tabContainer.find(".tab__item_active").removeClass("tab__item_active");
                $tab.addClass("tab__item_active");
            }
        }
    });
});

// tabs2
$(function () {

    $(document).delegate(".js-tabs-compl .tab-compl__item", "click", function(){
        var $tab = $(this);
        var $tabContainer = $tab.parent(".js-tabs-compl");
        var nameContent = $tab.data("name");

        if (nameContent && nameContent.length > 0) {

            var $newActiveTabContent = $tabContainer.find(".tab-compl-content_name-" + nameContent);
            if ($newActiveTabContent) {

                $tabContainer.find(".tab-compl-content_active").removeClass("tab-compl-content_active");
                $newActiveTabContent.addClass("tab-compl-content_active");

                $tabContainer.find(".tab-compl__item_active").removeClass("tab-compl__item_active");
                $tab.addClass("tab-compl__item_active");
            }
        }
    });
});

//accordeon
$(document).ready(function(){

    $('.complectation').click(function(event){
        event.stopPropagation();
        var status = this.getAttribute('data-status') || "hidden";
        if (status === 'hidden') {
            $('.complectation-content').removeClass('active')
            $(this).next().addClass('complectation-content__active');
            this.setAttribute('data-status', "shown");

        } else {
            $(this).next().removeClass('complectation-content__active');
            this.setAttribute('data-status', "hidden")
        }
    });

});


$(function() {
    'use strict';

    function filterData(inputId, dataSelector) {

        var entry = document.querySelector(inputId),
            infoBlock = document.querySelectorAll(dataSelector),
            txtValue;

        if (!entry || !infoBlock) {
            return false
        }

        function getInfo(cells) {
            return [].map.call(cells, function (el) {
                var valueEl = el.getAttribute("data-model");
                return valueEl;
            });
        }

        var infoTxt = getInfo(infoBlock);


        entry.addEventListener('change', function (event) {
            txtValue = this.value;
            if (txtValue === "all") {
                for (var i = 0, len = infoBlock.length; i < len; i++) {
                    infoBlock[i].style.display = '';

                }
			console.log(txtValue);
            } else {
                if (txtValue.length > 1) {

                    (function () {
                        var patt = new RegExp(txtValue, 'i');
                        for (var i = 0, n = infoTxt.length; i < n; i++) {
                            var testResult;
                            testResult = patt.test(infoTxt[i]);

                            //console.log(i + ' - ' + testResult);

                            if (testResult) {
                                infoBlock[i].style.display = '';

                            }
                            else {
                                infoBlock[i].style.display = 'none';
                            }
                        }
                    })();
                }
                else {
                    (function () {
                        for (var i = 0, n = infoTxt.length; i < n; i++) {
                            infoBlock[i].setAttribute('style', '');
                        }
                    })();
                }
            }
        });

    }

    filterData('#seletor', '.js-item-info');

});