<?php

    /* @var $complectations Complectation[] */
    require_once __DIR__ .'/../lib/complectations.php';
    $currentModel = 'qashqai';
    $complectations = Complectation::getFromExcel($currentModel);

    $monthRU = array('01' => 'января', '02' => 'февраля', '03' => 'марта', '04' => 'апреля', '05' => 'мая', '06' => 'июня', '07' => 'июля', '08' => 'августа', '09' => 'сентября', '10' => 'октября', '11' => 'ноября', '12' => 'декабря');
    $nextDay = date('j', strtotime(' +5 day'));
    $nextMonth = strtoupper($monthRU[date('m', strtotime(' +5 day'))]);
?>
<title>Купить Nissan Qashqai у официального дилера ОВОД</title>
<title>Купить Nissan Qashqai у официального дилера ОВОД</title>
<title>Купить Nissan Qashqai у официального дилера ОВОД</title>
<title>Купить Nissan Qashqai у официального дилера ОВОД</title>
<!DOCTYPE html>
<html lang="ru-RU">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE = edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="keywords" content="">
    <link rel="stylesheet" type="text/css" href="static/fonts/fonts.css">
    <link rel="stylesheet" type="text/css" href="static/css/main.css">
    <!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");
   ym(38306325, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        trackHash:true
   });
</script>

<noscript><div><img src="https://mc.yandex.ru/watch/38306325" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- Comagic -->
    <script type="text/javascript">
        var __cs = __cs || [];
        __cs.push(["setCsAccount", "PY0tS1FLCGBhGqlSPLDv8vk9kmLSpgEe"]);
    </script>
    <!-- /Comagic -->
    <title>Купить Nissan Qashqai у официального дилера ОВОД</title>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
      new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
      j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
      'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5GM8W2');</script>
    <!-- End Google Tag Manager -->
</head>

<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5GM8W2" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <div class="wrapper">
        <header>
            <div class="container">
                <div class="header">
                    <div class="header_dealer-logo">
                        <img src="./static/img/general/dealer_logo.png" alt="">
                    </div>
                    <div class="header_awards-logo">
                        <img src="static/img/general/logo_awards.jpg" alt="">
                    </div>
                    <div class="header_addres">
                        <a href="#map"><span class="street">Москва, 26 км МКАД (внешняя сторона), вл. 13</span></a>
                        <span class="phone"><a href="tel:+74957875353">+7 (495) 787-53-53</a></span>
                        <a class="js-popup-link btn btn-red" href="#feedback">Обратный звонок</a>
                    </div>
                    <div class="header_brand-logo">
                        <img src="./static/img/general/brand_logo.jpg" alt="">
                    </div>
                </div>
            </div>
        </header>
        <div class="wrapper">
            <!-- <section class="top_banner-wrap"><img class="top_banner_big" src="./static/img/banner/qashqai.png" alt=""> -->
            <section class="top_banner-wrap"><img class="top_banner_big" src="./static/img/banner/qashqai_june.jpg" alt="">
                <div class="top-banner__text" style="left: 56%;">
                    <div class="top-banner__date">
                        <span>ВЕСЬ<br> МАРТ
                            <!--
                <?= $nextDay ?> 
                <?= $nextMonth ?>
-->
                        </span>
                    </div>
                    <div class="top-banner__benefit">
                        <!--<span class="banner-benefits__left-text" style="font-size: 18px;">Солдатенкова Наталья <a href="mailto:Soldatenkova@ovod.ru">Soldatenkova@ovod.ru</a></span><br>-->
                        <span class="banner-benefits__left-text">ВАША ВЫГОДА ДО </span>
                        <span class="banner-benefits__right-text">
                            <span class="benefits-price">551 272 ₽</span>
                        </span>
                    </div>
                </div>
            </section><a class="scroll_link" id="spec"></a>
            <section class="promo_title-wrap gray_bg">
                <div class="container">
                    <div class="promo_title">
                        <span class="promo_title_top-text">
                            <span class="white_text">ВЕСЬ</span>
                            МАРТ
                            <!--
                <?= $nextDay ?> 
                <?= $nextMonth ?><br> 
-->
                            <span class="white_text">
                                <!--ДЕНЬ РОЖДЕНИЯ ОВОД!--> ВСЕ МАШИНЫ С ПТС БЕЗ НАЦЕНКИ ДИЛЕРА!</span>
                        </span>
                        <span class="main-benef__item"><span class="red_text">✔</span> Выгода до 551 272 ₽</span>
                        <span class="main-benef__item"><span class="red_text">✔</span> Кредит от 7 500 ₽/мес</span>
                        <span class="main-benef__item"><span class="red_text">✔</span> Кредит 0% на 3 года</span>
                        <span class="main-benef__item"><span class="red_text">✔</span> В наличии с ПТС</span>
                        <span class="main-benef__item"><span class="red_text">✔</span> Выгодный Trade-in</span>
                        <span class="main-benef__item"><span class="red_text">✔</span> Первоначальный взнос – 0%</span>
                        <span class="main-benef__item">
                            <!-- <span class="red_text">✔</span> Первоначальный взнос – 0% --></span>
                        <!-- <span class="main-benef__item"><span class="red_text">✔</span> Кредит – от 0%</span> -->
                        <form class="top_form" id="f-promo_title" action="" data-callkeeper_name="Получить лучшую цену!" onsubmit="yaCounter38306325.reachGoal('deistvie'); return true;">
                            <!--input type="text" name="name" placeholder="Имя" data-callkeeper="person"-->
                            <input type="text" name="phone" placeholder="Телефон" data-callkeeper="tel">
                            <div class="submit_wrap">
                                <input class="btn btn-red" type="submit" value="Получить выгоду">
                            </div>
                        </form>
                    </div>
                </div>
            </section><span class="section_title">Nissan QASHQAI по специальной цене</span>
            <div class="complect" id="complect_sentra" style="display: block;"><a name="Complect_list-sentra"></a>
                <div class="complect_left_right">
                    <div class="complect_left complect_block"><span class="model_title">Комплектации Nissan QASHQAI
                            <div class="complect_img">
                                <div class="tab js-tabs">
                                    <div class="tab-content tab-content_name-tab-1 tab-content_active"><img src="./static/img/content/cars/car_1.jpg"></div>
                                    <div class="tab-content tab-content_name-tab-2"><img src="./static/img/content/cars/car_2.jpg"></div>
                                    <div class="tab-content tab-content_name-tab-3"><img src="./static/img/content/cars/car_3.jpg"></div>
                                    <div class="tab-content tab-content_name-tab-4"><img src="./static/img/content/cars/car_4.jpg"></div>
                                    <div class="tab-content tab-content_name-tab-5"><img src="./static/img/content/cars/car_5.jpg"></div>
                                    <div class="tab-content tab-content_name-tab-6"><img src="./static/img/content/cars/car_6.jpg"></div>
                                    <div class="tab-content tab-content_name-tab-7"><img src="./static/img/content/cars/car_7.jpg"></div>
                                    <div class="tab-content tab-content_name-tab-8"><img src="./static/img/content/cars/car_8.jpg"></div>
                                    <!-- <div class="tab-content tab-content_name-tab-9"><img src="./static/img/content/cars/9.jpg"></div> -->
                                    <a class="tab__item tab__item_active" data-name="tab-1"></a>
                                    <a class="tab__item" data-name="tab-2"></a>
                                    <a class="tab__item" data-name="tab-3"></a>
                                    <a class="tab__item" data-name="tab-4"></a>
                                    <a class="tab__item" data-name="tab-5"></a>
                                    <a class="tab__item" data-name="tab-6"></a>
                                    <a class="tab__item" data-name="tab-7"></a>
                                    <a class="tab__item" data-name="tab-8"></a>
                                    <!-- <a class="tab__item" data-name="tab-9"></a> -->
                                </div>
                            </div></span></div>
                    <div class="complect_right complect_block"><span class="model_price">Цена от 1 070 000 ₽</span><span class="model_benefit">Кредит 0% на 3 года</span>
                        <div class="model_button-yellow">
                            <!-- dr start--><a class="js-popup-link" href="#best_price">Получить выгоду</a>
                            <!-- dr end-->
                        </div>
                        <div class="model_button-white"><a class="js-popup-link" href="#test-drive" style="margin-bottom: 20px;">ЗАПИСАТЬСЯ НА ТЕСТ-ДРАЙВ</a></div>
                        <div class="model_button-white"><a class="js-popup-link" href="#credit" style="margin-bottom: 20px;">РАССЧИТАТЬ ЛЬГОТНЫЙ КРЕДИТ</a></div>
                        <div class="model_button-white"><a class="js-popup-link" href="#change-auto" style="margin-bottom: 20px;">ВЫГОДНО ОБМЕНЯТЬ СВОЙ АВТО НА ЭТОТ</a></div>
                        <div class="model_button-white">
                            <!-- dr start--><a class="js-popup-link" href="#feedback" style="margin-bottom: 20px;">ЗАКАЗАТЬ ОБРАТНЫЙ ЗВОНОК БЕСПЛАТНО</a>
                            <!-- dr end-->
                        </div>
                    </div>
                </div>
                <!-- <div class="complect_list">
            <div class="tab-compl js-tabs-compl">
              <a class="tab-compl__item tab__item_active" data-name="tab-1">Welcome</a>
              <a class="tab-compl__item" data-name="tab-2">Comfort</a>
              <a class="tab-compl__item" data-name="tab-3">Tekna</a>
              <form id="filter_table" onsubmit="yaCounter38306325.reachGoal('deistvie'); return true;">
                <select id="seletor" name="models">
                  <option value="all">Выберите комплектацию</option>
                    <? foreach (array_unique(Complectation::getFromExcel($currentModel, true)) as $complectation): ?>
                        <option value="<?= $complectation ?>"><?= $complectation ?></option>
                    <? endforeach; ?>
                </select>
              </form>
      <table class="complectation-item">
        <thead>
        <tr>
          <td class="cell-first">Комплектация</td>
          <td class="cell-ather">Цвет</td>
          <td class="cell-ather">Год</td>
          <td class="cell-ather">Цена от</td>
          <td></td>
        </tr>
        </thead>
                <? foreach ($complectations as $complectation): ?>
                    <tr data-model="<?= $complectation->complectation ?>" class="cplectation-tr js-item-info">
                        <td data-lable="Комплектация" class="cell-first"><?= $complectation->model ?> <nobr><?= $complectation->complectation ?></nobr><br>
                            <span class="cell-first-text">Кредит от 8%</span><a class="cell-first-link js-popup-link" href="#credit"><nobr>Рассчитать кредит >></nobr></a>
                        </td>
                        <td data-lable="Цвет" class="cell-ather"><?= $complectation->color ?></td>
                        <td data-lable="Год" class="cell-ather"><?= $complectation->year ?></td>
                        <td data-lable="Цена от" class="cell-ather">
                            <? if ($complectation->price): ?>
                                <strike><?= number_format($complectation->price, 0, '', ' ') ?> ₽</strike>
                            <? endif ?>
                        </td>
                        <td data-lable="" class="cell-ather"><button class="js-popup-link" href="#best_price">Получить выгоду</button></td>
                    </tr>
                <? endforeach; ?>
      </table>



            </div>
          </div> -->
            </div><a class="scroll_link" id="models"></a>
            <section class="filter_section">
                    <div class="car-block">
                        <div class="car-block__container">
                            <div id="filter" class="filter">
                                <form action="" id="filter_form" class="filter_form">
                                    <div class="filter_form__groups">
                                        <div class="filter_form__group">
                                            <label for="">Модель
                                                <select name="form_model" id="form_model">
                                                    <option value="0" selected>Все</option>
                                                </select>
                                            </label>
                                        </div>
                                        <div class="filter_form__group">
                                            <label for="">Год
                                                <select name="form_year" id="form_year">
                                                    <option value="0" selected>Все</option>
                                                </select>
                                            </label>
                                        </div>
                                        <div class="filter_form__group">
                                            <label for="">Цвет
                                                <select name="form_color" id="form_color">
                                                    <option value="0" selected>Все</option>
                                                </select>
                                            </label>
                                        </div>
                                    </div><!-- 
                            <div class="filter_form__checkbox">
                                <label for="">
                                    <input type="checkbox" name="" id=""> Горячие предложения
                                </label>
                            </div> -->
                        </form>
                    </div>
                    <div class="car_list">
                    </div>
                </div>
            </div>
            </section>
            <section class="car_block-wrap">
                <style>
                    .car_item-buttons {
                    bottom: -80px;
                }
                .car_item .car_title:after {
                content: '';
                width: 28px;
                height: 5px;
                position: absolute;
                top: 30px;
                left: 0px;
                background: #c41733;
                }
                .car_item .corp:after {
                content: '';
                width: 28px;
                height: 5px;
                position: absolute;
                top: 80px;
                left: 0px;
                background: #c41733;
                }
                .car_item .comment {
                    color: black;
                    font-size: 18px;
                    font-weight: 400;
                    padding-top: 10px;
                    display: block;
                    text-align: left;
                    padding-bottom: 35px;
                }
                .car_item .comment span {
                    color: #c41733;
                    display: inline-block;
                }
                </style>
<!--                 <h2>Автомобили по специальным ценам</h2>
<div class="car-block">
    <div class="car-block__container">
        <div class="car_item-wrap">
            <div class="car_item" style="min-height:400px;">
                <a class="js-popup-link" href="#best_price">
                    <span class="car_title">NEW QASHQAI</span></a>
                <div class="comment">В наличии: <span>250 авто</span></div>
                <a class="js-popup-link" href="#best_price">
                    <div class="car_img_wrap"><img src="/promo/static/img/content/cars/qashqai_new.png" alt=""></div>
                    <div class="car_item-text" style="text-align: left;">
                        <span class="car_beneff" style="display: block !important;">&bull; Кредит от <b>7 500</b> ₽/мес</span>
                        <span class="car_beneff" style="display: block !important;">&bull; Субсидированный кредит</span>
                        <span class="car_beneff" style="display: block !important;">&bull; 3 ТО - БЕСПЛАТНО</span>
                        <span class="car_beneff" style="display: block !important;">&bull; 3 опции – ДАРИМ!</span>
                        <span class="car_beneff red_text" style="display: block !important;">&bull; Регистрация ТС в ГИБДД в Подарок!<sup>3</sup></span>
                    </div>
                </a>
                <div class="car_item-buttons">
                    <a class="js-popup-link btn btn-red" href="#best_price">Получить лучшую цену</a>
                    <a class="js-popup-link btn btn-red" href="#credit">Купить в кредит<sup></sup></a>
                </div>
            </div>
        </div>
        <div class="car_item-wrap">
            <div class="car_item" style="min-height:400px;"><a class="js-popup-link" href="#best_price"><span class="car_title">NEW X-TRAIL</span></a>
                <div class="comment">В наличии: <span>120 авто</span></div>
                <a class="js-popup-link" href="#best_price">
                    <div class="car_img_wrap"><img src="/promo/static/img/content/cars/xtrail_new.png" alt=""></div>
                    <div class="car_item-text" style="text-align: left;">
                        <span class="car_beneff " style="display:block !important;">&bull; Кредит от <b>9 000</b> ₽/мес</span>
                        <span class="car_beneff" style="display: block !important;">&bull; Субсидированный кредит</span>
                        <span class="car_beneff" style="display: block !important;">&bull; 3 ТО - БЕСПЛАТНО</span>
                        <span class="car_beneff" style="display: block !important;">&bull; 3 опции – ДАРИМ!</span>
                        <span class="car_beneff red_text" style="display: block !important;">&bull; Регистрация ТС в ГИБДД в Подарок!<sup>3</sup></span>
                    </div>
                </a>
                <div class="car_item-buttons">
                    <a class="js-popup-link btn btn-red" href="#best_price">Получить лучшую цену</a>
                    <a class="js-popup-link btn btn-red" href="#credit">Купить в кредит<sup></sup></a>
                </div>
            </div>
        </div>
        <div class="car_item-wrap">
            <div class="car_item" style="min-height:400px;">
                <a class="js-popup-link" href="#best_price">
                    <span class="car_title">Qashqai</span></a>
                <div class="comment">В наличии: <span>371 авто</span></div>
                <a class="js-popup-link" href="#best_price">
                    <div class="car_img_wrap"><img src="/promo/static/img/content/cars/qashqai.png" alt=""></div>
                    <div class="car_item-text" style="text-align: left;">
                        <span class="car_beneff" style="display: block !important;">&bull; Кредит от <b>7 500</b> ₽/мес</span>
                        <span class="car_beneff" style="display: block !important;">&bull; Субсидированный кредит</span>
                        <span class="car_beneff" style="display: block !important;">&bull; 3 ТО - БЕСПЛАТНО</span>
                        <span class="car_beneff" style="display: block !important;">&bull; 3 опции – ДАРИМ!</span>
                        <span class="car_beneff red_text" style="display: block !important;">&bull; Регистрация ТС в ГИБДД в Подарок!<sup>3</sup></span>
                    </div>
                </a>
                <div class="car_item-buttons">
                    <a class="js-popup-link btn btn-red" href="#best_price">Получить лучшую цену</a>
                    <a class="js-popup-link btn btn-red" href="#credit">Купить в кредит<sup></sup></a>
                </div>
            </div>
        </div>
        <div class="car_item-wrap">
            <div class="car_item" style="min-height:400px;"><a class="js-popup-link" href="#best_price"><span class="car_title">X-Trail</span></a>
                <div class="comment">В наличии: <span>190 авто</span></div>
                <a class="js-popup-link" href="#best_price">
                    <div class="car_img_wrap"><img src="/promo/static/img/content/cars/xtrail.png" alt=""></div>
                    <div class="car_item-text" style="text-align: left;">
                        <span class="car_beneff " style="display:block !important;">&bull; Кредит от <b>9 000</b> ₽/мес</span>
                        <span class="car_beneff" style="display: block !important;">&bull; Субсидированный кредит</span>
                        <span class="car_beneff" style="display: block !important;">&bull; 3 ТО - БЕСПЛАТНО</span>
                        <span class="car_beneff" style="display: block !important;">&bull; 3 опции – ДАРИМ!</span>
                        <span class="car_beneff red_text" style="display: block !important;">&bull; Регистрация ТС в ГИБДД в Подарок!<sup>3</sup></span>
                    </div>
                </a>
                <div class="car_item-buttons">
                    <a class="js-popup-link btn btn-red" href="#best_price">Получить лучшую цену</a>
                    <a class="js-popup-link btn btn-red" href="#credit">Купить в кредит<sup></sup></a>
                </div>
            </div>
        </div>
        <div class="car_item-wrap">
            <div class="car_item" style="min-height:400px;"><a class="js-popup-link" href="#best_price"><span class="car_title">Terrano</span></a>
                <div class="comment">В наличии: <span>50 авто</span></div>
                <a class="js-popup-link" href="#best_price">
                    <div class="car_img_wrap"><img src="/promo/static/img/content/cars/terrano.png" alt=""></div>
                    <div class="car_item-text" style="text-align: left;">
                        <span class="car_beneff " style="display:block !important;">&bull; Кредит от <b>5 500</b> ₽/мес</span>
                        <span class="car_beneff" style="display: block !important;">&bull; Субсидированный кредит</span>
                        <span class="car_beneff" style="display: block !important;">&bull; 3 ТО - БЕСПЛАТНО</span>
                        <span class="car_beneff" style="display: block !important;">&bull; 3 опции – ДАРИМ!</span>
                        <span class="car_beneff red_text" style="display: block !important;">&bull; Регистрация ТС в ГИБДД в Подарок!<sup>3</sup></span>
                    </div>
                </a>
                <div class="car_item-buttons">
                    <a class="js-popup-link btn btn-red" href="#best_price">Получить лучшую цену</a>
                    <a class="js-popup-link btn btn-red" href="#credit">Купить в кредит<sup></sup></a>
                </div>
            </div>
        </div>
        <div class="car_item-wrap">
       
        <div class="car_item" style="min-height:400px;"><a class="js-popup-link" href="#best_price"><span class="car_title">X-Trail</span></a>
            <div class="comment">В наличии: <span>190 авто</span></div>
            <a class="js-popup-link" href="#best_price">
                <div class="car_img_wrap"><img src="/promo/static/img/content/cars/xtrail.png" alt=""></div>
                <div class="car_item-text" style="text-align: left;">
                    <span class="car_beneff " style="display:block !important;">&bull; Кредит от <b>9 000</b> ₽/мес</span>
                    <span class="car_beneff" style="display: block !important;">&bull; Субсидированный кредит</span>
                    <span class="car_beneff" style="display: block !important;">&bull; 3 ТО - БЕСПЛАТНО</span>
                    <span class="car_beneff" style="display: block !important;">&bull; 3 опции – ДАРИМ!</span>
                    <span class="car_beneff red_text" style="display: block !important;">&bull; Регистрация ТС в ГИБДД в Подарок!<sup>3</sup></span>
                </div>
            </a>
            <div class="car_item-buttons">
                <a class="js-popup-link btn btn-red" href="#best_price">Получить лучшую цену</a>
                <a class="js-popup-link btn btn-red" href="#credit">Купить в кредит<sup></sup></a>
            </div>
        </div>
    </div>
        <div class="car_item-wrap">

      <div class="car_item" style="min-height:400px;"><a class="js-popup-link" href="#best_price"><span class="car_title">JUKE</span></a>
            <div class="comment">В наличии: <span>157 авто</span></div>

            <a class="js-popup-link" href="#best_price">
                <style>
                    .greyBG {
                        background-color: #dadada;
                    }
                </style>
                <div class="car_img_wrap"><img src="/promo/static/img/content/cars/JUKE.png" alt=""></div>

                <div class="car_item-text" style="text-align: left;">
                    <span class="car_beneff" style="display: block !important;">&bull; Кредит от <b>7 500</b> ₽/мес</span>
                    <span class="car_beneff" style="display: block !important;">&bull; Субсидированный кредит</span>
                    <span class="car_beneff" style="display: block !important;">&bull; 3 ТО - БЕСПЛАТНО</span>
                    <span class="car_beneff" style="display: block !important;">&bull; 3 опции – ДАРИМ!</span>
                    <span class="car_beneff red_text" style="display: block !important;">&bull; КАСКО<sup>2</sup> в Подарок!</span>
                    <span class="car_beneff red_text" style="display: block !important;">&bull; Регистрация ТС в ГИБДД в Подарок!<sup>3</sup></span>
                </div>
            </a>
            <div class="car_item-buttons">
                <a class="js-popup-link btn btn-red" href="#best_price">Получить лучшую цену</a>
                <a class="js-popup-link btn btn-red" href="#credit">Купить в кредит<sup></sup></a>
            </div>
        </div>
    </div>
        <div class="car_item-wrap">
            <div class="car_item" style="min-height:400px;"><a class="js-popup-link" href="#best_price"><span class="car_title">
                        1 а/мMurano</span></a>
                <div class="comment">В наличии: <span>40 авто</span></div>
                <a class="js-popup-link" href="#best_price">
                    <div class="car_img_wrap"><img src="/promo/static/img/content/cars/murano.png" alt=""></div>
                    <div class="car_item-text" style="text-align: left;">
                        <span class="car_beneff " style="display:block !important;">&bull; Выгода до <nobr><b>763 000</b> ₽/мес</nobr></span>
                        <span class="car_beneff" style="display: block !important;">&bull; От 0,1% на 2 года.</span>
                        <span class="car_beneff" style="display: block !important;">&bull; 3 ТО - БЕСПЛАТНО</span>
                        <span class="car_beneff" style="display: block !important;">&bull; 3 опции – ДАРИМ!</span>
                        <span class="car_beneff red_text" style="display: block !important;">&bull; Регистрация ТС в ГИБДД в Подарок!<sup>3</sup></span>
                    </div>
                </a>
                <div class="car_item-buttons">
                    <a class="js-popup-link btn btn-red" href="#best_price">Получить лучшую цену</a>
                    <a class="js-popup-link btn btn-red" href="#credit">Купить в кредит<sup></sup></a>
                </div>
            </div>
        </div>
        <div class="car_item-wrap">
            <div class="car_item corp" style="min-height:300px;"><a class="js-popup-link" href="#best_price"> <span class="car_title corp">ДЛЯ КОРПОРАТИВНЫХ КЛИЕНТОВ</span></a>
                <div class="comment"></div>
                <a class="js-popup-link" href="#best_price">
                    <div class="car_img_wrap"><img src="/promo/static/img/content/cars/corp.png" alt=""></div>
                    <div class="car_item-text" style="text-align: left;">
                        <span class="car_beneff" style="display: block !important;">&bull; Госсубсидия для лизинговых сделок от 10% до 12,5%</span>
                        <span class="car_beneff" style="display: block !important;">&bull; Субсидия суммируется с корпоративными скидками</span>
                        <span class="car_beneff" style="display: block !important;">&bull; Отсутствует ограничение по цвету автомобиля</span>
                        <span class="car_beneff" style="display: block !important;">&bull; Без полиса ОСАГО для такси</span>
                    </div>
                </a>
                <div class="car_item-buttons" style="bottom:-40px;">
                    <a class="js-popup-link btn btn-red" href="#best_price">Субсидированный лизинг на выгодных условиях</a>
                </div>
            </div>
        </div>
        <div class="car_item-wrap">
<div class="car_item" style="min-height:400px;"><span class="car_title">Sentra</span>
<div class="car_img_wrap"><img src="./static/img/content/cars/sentra.png" alt=""></div>
<div class="car_item-text"><span class="car_price greyBG">от 896 000 ₽</span>
<span class="car_beneff red_text" style="display: block !important;">Кредит 0% на 3 года<sup>1</sup></span>
</div>
<div class="car_item-buttons"><a class="js-popup-link btn btn-red" href="#best_price">Получить лучшую цену</a><a class="js-popup-link btn btn-black" href="#credit">Купить в кредит<sup></sup></a></div>
</div>
</div>
        <div class="car_item-wrap">
<div class="car_item" style="min-height:400px;"><span class="car_title">Pathfinder</span>
<div class="car_img_wrap"><img src="./static/img/content/cars/pathfinder.png" alt=""></div>
<div class="car_item-text"><span class="car_price greyBG">от 2 375 000 ₽</span><span class="car_beneff red_text" style="display: block !important;">Выгода до 380 000 ₽<sup>4</sup></span><span class="car_beneff red_text" style="display: block !important;">Кредит 12% на 3 года<sup>1</sup></span></div>
<div class="car_item-buttons"><a class="js-popup-link btn btn-red" href="#best_price">Получить лучшую цену</a><a class="js-popup-link btn btn-black" href="#credit">Купить в кредит<sup></sup></a></div>
</div>
</div>
        <div class="car_item-wrap">
<div class="car_item" style="min-height:400px;"><span class="car_title">Patrol</span>
<div class="car_img_wrap"><img src="./static/img/content/cars/patrol.png" alt=""></div>
<div class="car_item-text"><span class="car_price greyBG">от 3 965 000 Р</span><span class="car_beneff red_text" style="display: block !important;">Выгода до 585 000 Р<sup>4</sup></span><span class="car_beneff red_text" style="display: block !important;">Кредит 12% на 3 года<sup>1</sup></span></div>
<div class="car_item-buttons"><a class="js-popup-link btn btn-red" href="#best_price">Получить лучшую цену</a><a class="js-popup-link btn btn-black" href="#credit">Купить в кредит<sup></sup></a></div>
                        </div>
                    </div>
    </div>
</div> -->
            </section> 

            <a class="scroll_link" id="preim"></a>
            <section class="our_advantages-wrap gray_bg">
                <div class="our-advantages">
                    <h2>Наши преимущества</h2>
                    <ul class="advantages_list">
                        <li style="    padding-top: 23px;">
                            <p style="text-align: center; vertical-align: middle;"><span>Официальный дилер<br> с 2000 года</span></p>
                        </li>
                        <li><span class="inline">от</span><span class="advantages_title inline">&nbsp;0%<sup></sup></span><span>Кредит</span></li>
                        <li class="only_text"><span style="font-size: 12px;">Гарантия лучшей цены! Перебиваем любое предложение!</span></li>
                        <li><span class="advantages_title">ПТС</span><span>Авто в наличии с ПТС</span></li>
                        <li><span class="advantages_title">Trade-in</span><span>Выгодный обмен авто на новый</span></li>
                        <li class="only_text"><span>Гарантия 5 лет или 150 000 км</span></li>
                        <li class="only_text"><span>Оплата дороги до ДЦ. Оформление за 60 минут. </span></li>
                        <li class="only_text"><span>Регистрация в ГИБДД в подарок! </span></li>
                    </ul>
                </div>
            </section><a class="scroll_link" id="contacts"></a>
            <section class="bottom_open_form-wrap">
                <div class="container">
                    <div class="bottom_open_form">
                        <h2>Получите лучшее предложение</h2>
                        <form class="bottom_form" id="f-bottom_open_form" action="" onsubmit="yaCounter38306325.reachGoal('deistvie'); return true;">
                            <!--     <input type="text" name="name" placeholder="Ваше имя" data-callkeeper="person"> -->
                            <input type="text" name="phone" placeholder="Ваш телефон" data-callkeeper="tel">
                            <div class="submit_wrap">
                                <input class="btn btn-red" type="submit" value="Отправить">
                            </div>
                        </form>
                    </div>
                </div>
                <div id="map"></div>
            </section>
            <section class="disclaimer-wrap">
                <p>
                    О наличии автомобилей уточняйте в дилерских центрах. Не является офертой. <br>
                    Компания оставляет за собой право изменять или отменять условия данного предложения <br>
                    Условия расчета ежемесячного платежа уточняйте в отделах кредитования. .+74957875353
                </p>
            </section>
        </div>
        <div class="popup-form mfp-hide" id="feedback"><span class="popup-form__title">ЕСТЬ ВОПРОСЫ? ОСТАВЬТЕ ТЕЛЕФОН!</span>
            <form id="f-feedback" action="" data-callkeeper_name="Заказать обратный звонок!" onsubmit="yaCounter38306325.reachGoal('deistvie'); return true;">
                <!-- <input type="text" name="name" placeholder="Ваше имя" data-callkeeper="person"> -->
                <input type="text" name="phone" placeholder="Ваш телефон" data-callkeeper="tel">
                <input class="btn btn-red" type="submit" value="ПЕРЕЗВОНИТЕ МНЕ">
                <span class="form-discl">Нажав кнопку «Отправить», я даю согласие на обработку <a class="under" target="_blank" href="https://ovod-nissan.ru/about/about-us/politika-v-otnoshenii-obrabotki-personalnyh-dannyh-avtocentra-ovod.php"> моих персональных данных и получение рекламы. </a></span>
            </form>
        </div>
        <div class="popup-form mfp-hide" id="best_price"><span class="popup-form__title">Желаете выгоду больше?<br>Закажите обратный звонок!</span>
            <form id="f-best_price" action="" data-callkeeper_name="Получить лучшую цену!" onsubmit="yaCounter38306325.reachGoal('deistvie'); return true;">
                <!--  <input type="text" name="name" placeholder="Ваше имя" data-callkeeper="person"> -->
                <input type="text" name="phone" placeholder="Ваш телефон" data-callkeeper="tel">
                <input class="btn btn-red" type="submit" value="ПОЛУЧИТЬ ВЫГОДУ">
                <span class="form-discl">Нажав кнопку «Отправить», я даю согласие на обработку <a class="under" target="_blank" href="https://ovod-nissan.ru/about/about-us/politika-v-otnoshenii-obrabotki-personalnyh-dannyh-avtocentra-ovod.php"> моих персональных данных и получение рекламы. </a></span>
            </form>
        </div>
        <div class="popup-form mfp-hide" id="test-drive"><span class="popup-form__title">Хотите пройти тест-драйв на этом автомобиле?</span>
            <form id="f-best_price" action="" data-callkeeper_name="Получить лучшую цену!" onsubmit="yaCounter38306325.reachGoal('deistvie'); return true;">
                <!--   <input type="text" name="name" placeholder="Ваше имя" data-callkeeper="person"> -->
                <input type="text" name="phone" placeholder="Ваш телефон" data-callkeeper="tel">
                <input class="btn btn-red" type="submit" value="ЗАПИСАТЬСЯ НА ТЕСТ-ДРАЙВ">
                <span class="form-discl">Нажав кнопку «Отправить», я даю согласие на обработку <a class="under" target="_blank" href="https://ovod-nissan.ru/about/about-us/politika-v-otnoshenii-obrabotki-personalnyh-dannyh-avtocentra-ovod.php"> моих персональных данных и получение рекламы. </a></span>
            </form>
        </div>
        <div class="popup-form mfp-hide" id="credit"><span class="popup-form__title">Интересует покупка в кредит? Получите выгодное кредитное предложение прямо сейчас!</span>
            <form id="f-credit" action="" data-callkeeper_name="Заявка на кредит!" onsubmit="yaCounter38306325.reachGoal('deistvie'); return true;">
                <!-- <input type="text" name="name" placeholder="Ваше имя" data-callkeeper="person"> -->
                <input type="text" name="phone" placeholder="Ваш телефон" data-callkeeper="tel">
                <input class="btn btn-red" type="submit" value="ПОЛУЧИТЬ ИНФОРМАЦИЮ">
                <span class="form-discl">Нажав кнопку «Отправить», я даю согласие на обработку <a class="under" target="_blank" href="https://ovod-nissan.ru/about/about-us/politika-v-otnoshenii-obrabotki-personalnyh-dannyh-avtocentra-ovod.php"> моих персональных данных и получение рекламы. </a></span>
            </form>
        </div>
        <div id="payment" class="popup-form mfp-hide">
            <span class="popup-form__title">Интересует покупка в рассрочку? Получите выгодное предложение прямо сейчас!</span>
            <form action="" id="f-payment" data-callkeeper_name="Заявка на кредит!" onsubmit="yaCounter38306325.reachGoal('deistvie'); return true;">
                <!-- <input type="text" name="name" placeholder="Ваше имя" data-callkeeper="person"> -->
                <input type="text" name="phone" placeholder="Ваш телефон" data-callkeeper="tel">
                <input type="submit" value="ПОЛУЧИТЬ ИНФОРМАЦИЮ" class="btn btn-red">
            </form>
            <span class="form-discl">Нажав кнопку «Отправить», я даю согласие на обработку <a class="under" target="_blank" href="https://ovod-nissan.ru/about/about-us/politika-v-otnoshenii-obrabotki-personalnyh-dannyh-avtocentra-ovod.php"> моих персональных данных и получение рекламы. </a></span>
        </div>
        <div class="popup-form mfp-hide" id="change-auto"><span class="popup-form__title">УЗНАЙТЕ, КАК ВЫГОДНО ОБМЕНЯТЬ СВОЙ АВТО НА НОВЫЙ NISSAN</span>
            <form id="f-insurance" action="" data-callkeeper_name="Получить КАСКО!" onsubmit="yaCounter38306325.reachGoal('deistvie'); return true;">
                <!--    <input type="text" name="name" placeholder="Ваше имя" data-callkeeper="person"> -->
                <input type="text" name="phone" placeholder="Ваш телефон" data-callkeeper="tel">
                <input class="btn btn-red" type="submit" value="ПОЛУЧИТЬ ИНФОРМАЦИЮ">
                <span class="form-discl">Нажав кнопку «Отправить», я даю согласие на обработку <a class="under" target="_blank" href="https://ovod-nissan.ru/about/about-us/politika-v-otnoshenii-obrabotki-personalnyh-dannyh-avtocentra-ovod.php"> моих персональных данных и получение рекламы. </a></span>
            </form>
        </div>
        <div class="popup-form mfp-hide" id="sucess"><span class="popup-form__title">Спасибо. Ваша заявка отправлена</span></div>
    </div>
    <script src="static/js/jquery.js"></script>
    <script src="static/js/libs.min.js"></script>
    <script src="static/js/json.js"></script>
    <script src="static/js/main.js"></script>
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU"></script>
    <!-- calltouch -->
    <script src="https://mod.calltouch.ru/init.js?id=6joxcflo"></script>
    <!-- /calltouch -->

    <script type="text/javascript" async src="https://app.comagic.ru/static/cs.min.js"></script>
    <script async src="//app.konget.ru/inject?token=d9ae2e39eb2c4c7a86352789faf92398"></script>
</body>

</html>