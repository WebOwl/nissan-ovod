

$(document).ready(function () {
    $('[data-href]').click(function (e) {
        if ($(e.target).attr('href')) return;
        e.preventDefault();
        var target = $($(this).attr('data-href'));

        $('html, body').animate({
            scrollTop : target.offset().top - $('header').height() - 25
        }, 550);
    })
});