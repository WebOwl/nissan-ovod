(function () {
        var width = $(window).width();
        var params = {
            "width": "10",
            "radius": "60",
            "line": "solid",
            "line-color": "#facb55",
            "background": "solid",
            "background-color": "rgba(255,255,255,0.13)",
            "direction": "direct",
            "number-font-family": {
                "family": "Open Sans",
                "link": "<link href='//fonts.googleapis.com/css?family=Open+Sans&subset=latin,cyrillic' rel='stylesheet' type='text/css'>"
            },
            "number-font-size": "50",
            "number-font-color": "#ffffff",
            "separator-margin": "22",
            "separator-on": false,
            "separator-text": ":",
            "text-on": true,
            "text-font-family": {
                "family": "Open Sans",
                "link": "<link href='//fonts.googleapis.com/css?family=Open+Sans&subset=latin,cyrillic' rel='stylesheet' type='text/css'>"
            },
            "text-font-size": "15",
            "text-font-color": "#ffffff"
        };
        if (width < 992) {
            params = {
                "width": "3",
                "radius": "24",
                "line": "solid",
                "line-color": "#facb55",
                "background": "solid",
                "background-color": "rgba(255,255,255,0.13)",
                "direction": "direct",
                "number-font-family": {
                    "family": "Open Sans",
                    "link": "<link href='//fonts.googleapis.com/css?family=Open+Sans&subset=latin,cyrillic' rel='stylesheet' type='text/css'>"
                },
                "number-font-size": "16",
                "number-font-color": "#ffffff",
                "separator-margin": "3",
                "separator-on": false,
                "separator-text": ":",
                "text-on": true,
                "text-font-family": {
                    "family": "Open Sans",
                    "link": "<link href='//fonts.googleapis.com/css?family=Open+Sans&subset=latin,cyrillic' rel='stylesheet' type='text/css'>"
                },
                "text-font-size": "10",
                "text-font-color": "#ffffff"
            }
        }
        var _id = "6e52950e697d72abcf106e4b0efdd655";
        while (document.getElementById("timer" + _id)) _id = _id + "0";
        document.write("<div id='timer" + _id + "' style='min-width:290px;'></div>");
        var _t = document.createElement("script");
        _t.src = "static/js/timer.min.js";

        var _f = function (_k) {
            var l = new MegaTimer(_id, {
                "view": [1, 1, 1, 1],
                "type": {
                    "currentType": "3",
                    "params": {
                        "weekdays": [1, 1, 1, 1, 1, 1, 1],
                        "usertime": true,
                        "time": "00:00",
                        "tz": -180,
                        "hours": "96",
                        "minutes": "0"
                    }
                },
                "design": {
                    "type": "circle",
                    "params": params
                }, "designId": 8, "theme": "black"
            });

            if (_k != null) l.run();
        };
        _t.onload = _f;
        _t.onreadystatechange = function () {
            if (_t.readyState == "loaded") _f(1);
        };
        var _h = document.head || document.getElementsByTagName("head")[0];
        _h.appendChild(_t);
    }
).call(this);