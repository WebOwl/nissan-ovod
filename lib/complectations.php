<?php
require_once __DIR__ . '/../lib/PHPExcel.php';

/**
 * @property string $number
 * @property string $model
 * @property string $complectation
 * @property string $code
 * @property string $kpp
 * @property string $engine
 * @property string $vin
 * @property string $color
 * @property string $year
 * @property string $price
 */
class Complectation
{

    const NUMBER_INDEX = 1;
    const MODEL_INDEX = 2;
    const COMPLECTATION_INDEX = 3;
    const CODE_INDEX = 4;
    const KPP_INDEX = 5;
    const ENGINE_INDEX = 6;
    const VIN_INDEX = 7;
    const COLOR_INDEX = 8;
    const YEAR_INDEX = 10;
    const PRICE_INDEX = 11;

    protected static $complectationsFile = 'complectations.xls';
    protected $data = array();

    public function __construct($data)
    {
        $this->data = $data;
    }

    public static function getFromExcel($model, $getList = false)
    {
        $xsl = PHPExcel_IOFactory::load(__DIR__ . '/' . self::$complectationsFile);
        $sheet = $xsl->getActiveSheet();
        $data = array();
        foreach ($sheet->getRowIterator(2) as $row) {
            $compl = array();
            $cellIterator = $row->getCellIterator();
            foreach ($cellIterator as $cell) {
                $compl[] = $cell->getFormattedValue();
            }
            if (stripos($compl[1], $model) !== false) {
                $data[] = $getList ? $compl[self::COMPLECTATION_INDEX - 1] : new static($compl);
            }
        }
        return $data;
    }

    public function getList()
    {
        $data = array();
        foreach ($this->data as $complectation) {
            $data[] = $complectation[self::MODEL_INDEX - 1];
        }
    }

    public function __get($name)
    {
        $key = strtoupper($name . '_INDEX');
        $index = constant(get_class($this) . '::' . $key) - 1;
        if (!empty($this->data[$index])) {
            return $this->data[$index];
        }
        return false;
    }

}
