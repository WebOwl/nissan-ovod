<?php define('DOCROOT', $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR); ?>
<?php include 'content/seo.php'; ?>
<?php include 'content/cars-list.php'; ?>
<?php include 'content/disclaimers.php'; ?>
<?php include 'content/stock.php'; ?>
<?php include 'content/slides.php'; ?>
<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    <title>Выгода в автосалоне ОВОД NISSAN</title>
    <meta name="description" content="Выгодное приобретение автомобиля Ниссан в автосалоне ОВОД NISSAN" />
    <link rel="icon" href="favicon.ico">
    <link rel="stylesheet" href="static/css/style.css?11">
    <!-- <link rel="stylesheet" href="static/css/ny.css?11"> -->
    <meta name="viewport" content="width=device-width,height=device-height,user-scalable=no" />
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />
    <link rel="stylesheet" href="static/css/slick-theme.css">
    <link rel="stylesheet" href="static/css/custom.css">
    <script src="static/js/jquery.min.js"></script>
    <!-- Google Tag Manager -->
    <script>
        (function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-5GM8W2');
    </script>
    <!-- End Google Tag Manager -->
    <script type="text/javascript">
        var __cs = __cs || [];
        __cs.push(["setCsAccount", "PY0tS1FLCGBhGqlSPLDv8vk9kmLSpgEe"]);
    </script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");
   ym(38306325, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        trackHash:true
   });
</script>

<noscript><div><img src="https://mc.yandex.ru/watch/38306325" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
</head>

<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5GM8W2" height="0" width="0"
            style="display:none;visibility:hidden"></iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript) -->

    <header class="header">
        <div class="container header-container">
            <div class="header-item" js-popup>
                <div class="image image__logo">
                    <img src="static/img/dealer_logo.png" alt="">
                </div>
            </div>
            <div class="header-item" js-popup>
                <div class="image image__awards">
                    <img src="static/img/logo_awards.png" alt="">
                </div>
            </div>
            <div class="header-item header-block header-block_full">
                <p class="name" data-href="#map">Москва, 26 км МКАД (внешняя сторона), вл.&nbsp;13</p>
                <p class="phone"><a href="tel:+74957875353">+7 (495) 787-53-53</a></p>
            </div>
            <div class="header-item header-item_logo">
                <div class="image image__brand">
                    <img src="static/img/brand-logo.jpg" alt="">
                </div>
            </div>
            <div class="btn btn_head btn--small" js-popup>Обратный звонок</div>
        </div>
    </header>

    <main>
        <div class="section-main">
            <div class="top-slider">

                <?php foreach ($slides as $slide): ?>
                <div class="slide_item" js-popup data-title="Забронировать выгоду">
                    <div class="img-wrap">
                        <div class="img" style="background-image: url(static/img/slider/<?= $slide['image'] ?>)"></div>
                    </div>
                    <div class="text">
                        <?= $slide['text'] ?>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="section section-content section_dark">
            <div class="container">
                <div class="timer">
                    <p><b>До конца акции осталось:</b></p>
                    <script>
                        window.timestamp = JSON.parse('<?= json_encode($timestamp) ?>');
                    </script>
                    <script src="static/js/timer.js"></script>
                    <div class="car-list-block">
                        <div class="btn mb-2" js-popup data-title="Забронировать выгоду">Забронировать выгоду</div>

                    </div>
                </div>
            </div>
        </div>
        <div class="section section-content ny-theme">
            <div class="container">
                <div class="car-list">
                    <?php foreach ($cars as $car) : ?>
                    <div class="car-list__item" id="<?= $car['anchor'] ?>">
                        <div class="car-list-block">
                            <p class="name" js-popup><?= $car['new'] ? 'Новый ' : '' ?>
                                NISSAN <?= strtoupper($car['name']) ?></p>
                            <div class="image image__car" js-popup><img
                                    src="./static/img/models/<?= $car['dir'] ?>/<?= $car['image'] ?>" alt=""></div>
                        </div>
                        <div class="car-list-block">

                            <?php if ($car['name'] == 'Qashqai') : ?>
                            <p>Более <a class="link link--border" data-model="<?= $car['model'] ?>">7 автомобилей</a>
                                с ПТС</p>
                            <?php elseif ($car['name'] == 'X-Trail' ) : ?>
                            <p>Более <a class="link link--border" data-model="<?= $car['model'] ?>">12 автомобилей</a>
                                с ПТС</p>
                            <?php else : ?>
                            <p>Последние <a class="link link--border"
                                    data-model="<?= $car['model'] ?>"><?= $car['count'] ?> авто</a> на специальных
                                условиях</p>
                            <?php endif; ?>
                            <div class="btn mb-2" js-popup data-title="Узнать цену по новым условиям">Узнать цену по
                                новым условиям
                            </div>
                            <p class="footnote" js-popup data-title="Да, сделаем Вам лучшее предложение">Есть
                                предложение? <a href="#" class="link link--current link--border">
                                    <b>
                                        <nobr>Сделаем лучше</nobr>
                                    </b>
                                </a></p>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>

        <div class="section section__content content ny-relative">
            <!-- <img class="bgny-left" src="static/img/themeny/bgny-left.png" alt=""> -->
            <!-- <img class="bgny-right" src="static/img/themeny/bgny-right.png" alt=""> -->
            <div class="container">
                <h2 class="content-block-title" style="text-align: center;">Преимущества покупки автомобиля в ОВОД
                    NISSAN</h2>
                <div class="benefits-list">
                    <div class="benefits-list__item">
                        <div class="image image__benefit--big">
                            <img src="static/img/svg/am.svg" alt="">
                        </div>
                        <div class="benefits-list__text">
                            Огромный выбор&nbsp;а/м в&nbsp;наличии с&nbsp;ПТС!
                        </div>
                    </div>
                    <div class="benefits-list__item">
                        <div class="image image__benefit--big">
                            <img src="static/img/svg/bank.svg" alt="">
                        </div>
                        <div class="benefits-list__text">
                            Официальный дилер
                            с&nbsp;2000&nbsp;года
                        </div>
                    </div>
                    <div class="benefits-list__item">
                        <div class="image image__benefit--big">
                            <img src="static/img/svg/kasko.svg" alt="">
                        </div>
                        <div class="benefits-list__text">
                            Регистрация в&nbsp;ГИБДД в&nbsp;подарок
                        </div>
                    </div>
                    <div class="benefits-list__item">
                        <div class="image image__benefit--big">
                            <img src="static/img/svg/thumbs.svg" alt="">
                        </div>
                        <div class="benefits-list__text">
                            Выгодный обмен авто на&nbsp;новый
                        </div>
                    </div>
                    <div class="benefits-list__item">
                        <div class="image image__benefit--big">
                            <img src="static/img/ticket.png" alt="">
                        </div>
                        <div class="benefits-list__text">
                            Компенсируем затраты на&nbsp;дорогу покупателям из&nbsp;других регионов
                        </div>
                    </div>
                    <div class="benefits-list__item">
                        <div class="image image__benefit--big">
                            <img src="static/img/svg/to.svg" alt="">
                        </div>
                        <div class="benefits-list__text">
                            Гарантия 5 лет <nobr>или 150 000 км</nobr>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section section__content ny-relative">
            <!-- <img class="bgny2-left" src="static/img/themeny/bgny2-left.png" alt=""> -->
            <!-- <img class="bgny2-right" src="static/img/themeny/bgny2-right.png" alt=""> -->
            <div class="container">
                <h2 class="content-block-title" style="text-align: center;">Автомобили в наличии</h2>
                <form id="filter" class="form filter">
                    <div class="form-group">
                        <select name="model">
                            <option value="">Выберите модель</option>
                            <?php foreach ($models as $model) : ?>
                            <option value="<?= $model ?>"><?= $model ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <select name="compl" disabled>
                            <option value="">Выберите комплектацию</option>
                        </select></div>
                    <div class="form-group">
                        <select name="kpp" disabled>
                            <option value="">Выберите КПП</option>
                        </select></div>
                    <div class="form-group">
                        <select name="color" disabled>
                            <option value="">Выберите цвет</option>
                        </select></div>

                    <input type="hidden" name="models" value='<?= json_encode($models) ?>'>
                </form>
                <div class="filter-list car-list">
                    <?php $_stock = array_splice($stock, 0, 8); ?>
                    <?php foreach ($_stock as $car) : ?>
                    <div class="car-list__item car-list__item_fourth stock-item" js-popup="">
                        <div class="stock-item__block">
                            <div class="image image__car"><img src="./static/img/stock/<?= $car['image'] ?>" alt="">
                            </div>
                            <!--                            <div class="image image__car">--><?php //print_r(str_replace(" ","_",$car["image"])); ?>
                            <!--<img src="./static/img/stock/-->
                            <?//= str_replace(" ","_",$car['image']) ?>
                            <!--" alt=""></div>-->
                            <p class="stock-item__name"><?= $car['model'] ?></p>
                            <p><?= $car['compl'] ?></p>
                            <div class="footnote stock-item__info">
                                <?= $car['year'] . ' г.в. , ' . ($car['kpp'] ? ('КПП ' . $car['kpp'] . ', ') : '') . ($car['engine'] ? ($car['engine'] . ',') : '') . ' цвет кузова ' . $car['color'] ?>
                            </div>
                        </div>
                        <div class="stock-item__block">
                            <p class="availability">В наличии</p>
                            <div class="btn btn_full" data-title="Узнать цену по новым условиям" js-popup="">Узнать цену
                                по новым условиям
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
        <div class="content content--reverse-mobile content--half ny-relative" id="contacts">
            <!-- <img class="bgny3-left" src="static/img/themeny/bgny3-left.png" alt=""> -->
            <div class="content-block content-block--blank ny-relative">
                <!-- <img class="bgny3-right" src="static/img/themeny/bgny3-right.png" alt=""> -->
                <div class="map-cont" id="map"></div>
            </div>
            <div class="content-block content-block--text contacts-block">
                <h2 class="content-block-title"><span>ОВОД НИССАН</span></h2>
                <div class="content-block-item">
                    <p class="name">Адрес:</p>
                    <p>Москва, 26 км МКАД (внешняя сторона), вл. 13</p>
                    <p class="name">Телефон:</p>
                    <a class="link link--big mb-2" href="tel:+74957875353">+7 (495) 787-53-53</a>
                    <p class="name">Время работы:</p>
                    <p>Ежедневно с 9:00 до 21:00</p>
                    <p>Компенсируем затраты на&nbsp;дорогу покупателям из&nbsp;других регионов</p>
                </div>
                <div class="content-block-item">
                    <div class="btn" js-popup>Заказать звонок</div>
                </div>
            </div>
        </div>
        <div class="pb-2">
            <div class="container">
                <div class="mb-3" style="text-align: center">
                    <a href="#" class="link link--current link--border" agreement>Согласие на&nbsp;обработку
                        персональных
                        данных и&nbsp;получение рекламы</a>
                </div>
                <div class="mb-3" style="text-align: center">
                    <a class="link link--border" disclaimer-show><span style="display:none;">Скрыть</span>
                        <span>Подробные</span> условия акции</a>
                </div>
                <div class="disclaimer footnote cutted" style="height:0">
                    <?= $disclaimers ?>
                </div>
                <div class="mb-3" style="text-align: center">
                    <p>© <?=date('Y')?> Все права защищены.</p>
                </div>
            </div>
        </div>
    </main>

    <footer class="footer">
        <div class="container footer-container">
            <div class="footer-item footer__social">
                <a href="https://autodrive-agency.ru" class="main" target="_blank">
                    <img src="static/img/svg/autodrive.svg" alt="autodrive-agency">
                </a>
            </div>
        </div>
    </footer>
    <div class="text-popup" id="base">
        <div class="text-popup-wrap">
            <div class="close"></div>
            <div class="text-popup-content">

            </div>
            <div>
                <button class="btn" js-popup>Связаться с нами</button>
            </div>
        </div>
    </div>

    <div class="text-popup" id="agreement">
        <div class="text-popup-wrap">
            <div class="close"></div>
            <div class="text-popup-content">
                <h1 class="text-popup-title">Политика конфиденциальности</h1>
                <h3>Согласие на обработку персональных данных</h3>
                <p>Настоящим даю свое безусловное согласие ООО &laquo;Ниссан Мэнуфэкчуринг РУС&raquo; (далее по тексту
                    &ndash; Компания, местонахождение: Российская Федерация, 194362 Санкт-Петербург, пос. Парголово,
                    Комендантский пр., д. 140) на обработку моих персональных данных указанных выше (далее - ПДн)
                    свободно, своей волей и в своем интересе на следующих условиях. Обработка ПДн осуществляется в
                    целях: доставки заказанного товара, послепродажного обслуживания товара, уведомления о сервисных и
                    отзывных кампаниях; осуществления контроля продаж и обслуживания покупателей; хранения в
                    информационных системах для оптимизации процессов взаимодействия с покупателями; технической
                    поддержки информационных систем; статистических и аналитических целей; проведения маркетинговых
                    исследований. Настоящее согласие предоставляется на осуществление любых действий в отношении моих
                    ПДн, которые необходимы или желаемы для достижения указанных выше целей, включая (без ограничения)
                    сбор, систематизацию, накопление, хранение, уточнение (обновление, изменение), использование,
                    распространение (в том числе передача третьим лицам), обезличивание, блокирование, уничтожение,
                    трансграничную передачу ПДн в любой форме, а также осуществление любых иных действий с моими ПДн с
                    учетом законодательства РФ. Обработка вышеуказанных ПДн осуществляется путем смешанной обработки
                    (без использования средств автоматизации и с использованием таких средств), и осуществляется как в
                    информационных системах ПДн, так и вне таких информационных систем. Настоящим подтверждаю, что в
                    указанных выше целях даю согласие на передачу Компанией моих ПДн третьим лицам (обработчикам),
                    включая, но не ограничиваясь: компаниям группы Nissan, авторизованным дилерам (Nissan, Infiniti,
                    Datsun), а также организациям, с которыми Компания осуществляет взаимодействие на основании
                    соответствующих договоров (соглашений). Настоящим подтверждаю, что уведомлен о том, что могу
                    запросить у Компании актуальную информацию о третьих лицах (наименование или фамилию, имя, отчество
                    и адрес лица), которым осуществляется передача моих ПДн.</p>
                <p>Настоящее согласие действует в течение 25 лет со дня его получения. Вы также уведомляетесь о том, что
                    в соответствии со статьей 9 Федерального закона от 27.07.2006 г. № 152-ФЗ &laquo;О персональных
                    данных&raquo; настоящее согласие может быть отозвано путем направления в письменной форме
                    уведомления Компании заказным почтовым отправлением с описью вложения по адресу: 194362, г.
                    Санкт-Петербург, пос. Парголово, Комендантский проспект, д. 140, либо вручения лично под подпись
                    уполномоченным представителям Компании.</p>
                <h3>Согласие на рекламную коммуникацию</h3>
                <p>Настоящим Вы выражаете свое безусловное согласие ООО &laquo;Ниссан Мэнуфэкчуринг РУС&raquo; (далее -
                    &laquo;Общество&raquo;) на обработку вышеуказанных персональных данных с использованием и без
                    использования средств автоматизации, включая их передачу, в том числе трансграничную, компаниям
                    группы Nissan, авторизованным дилерам (Nissan, Infiniti, Datsun), а также организациям, с которыми
                    Общество осуществляет взаимодействие на основании соответствующих договоров (соглашений), для
                    следующих целей: доставки заказанного товара, послепродажного обслуживания товара, уведомления о
                    сервисных и отзывных кампаниях; осуществления контроля продаж и обслуживания покупателей; хранения в
                    информационных системах для оптимизации процессов взаимодействия с покупателями; технической
                    поддержки информационных систем; статистических и аналитических целей; проведения маркетинговых
                    исследований. Настоящее согласие действует в течение 25 лет со дня его получения. Вы также
                    уведомляетесь о том, что в соответствии со статьей 9 Федерального закона от 27.07.2006 г. № 152-ФЗ
                    &laquo;О персональных данных&raquo; настоящее согласие может быть отозвано путем направления в
                    письменной форме уведомления Обществу заказным почтовым отправлением с описью вложения по адресу:
                    194362, г. Санкт-Петербург, пос.Парголово, Комендантский проспект, д.140, либо вручения лично под
                    подпись уполномоченным представителям Общества.<br /> Настоящим Вы также подтверждаете, что согласны
                    получать информацию о товарах, услугах и мероприятиях с помощью средств связи (интернет, смс,
                    телефонных звонков, почты).</p>
            </div>
        </div>
    </div>

    <div class="popup popup-wrap" id="popup">
        <div class="popup-container">
            <div class="popup-close"></div>
            <h2 class="popup-title">Оставить заявку на обратный звонок</h2>
            <form class="form" novalidate>
                <div class="form-group">
                    <input type="tel" name="phone" placeholder="Ваш Телефон" required>
                </div>
                <div class="form-group">
                    <button class="button btn" type="submit"
                        onclick="Comagic.addOfflineRequest({phone: $('[name = phone]').val()});">Отправить</button>
                </div>
                <p class="footnote">
                    Отправляя форму, я даю согласие на&nbsp;обработку моих персональных данных и&nbsp;получение
                    рекламы. С&nbsp;условиями обработки персональных данных и&nbsp;получения рекламы, изложенными
                    на&nbsp;сайте
                    (<a href="#" class="link link--current link--border" agreement>Согласие на&nbsp;обработку
                        персональных
                        данных и&nbsp;получение рекламы</a>) — ознакомлен(а)
                    и&nbsp;согласен(на).</p>
            </form>
        </div>
    </div>

    <div id="success">
        <div class="form-messages-close"></div>
        <h3 class="popup-title">Ваша заявка отправлена</h3>
        <p class="popup-subtitle">Мы свяжемся с Вами в самое ближайшее время.</p>
        <div class="button btn">Закрыть окно</div>
    </div>

    <div id="error">
        <div class="form-messages-close"></div>
        <h3 class="popup-title">Произошла ошибка</h3>
        <p class="popup-subtitle">Попробуйте повторить отправку позднее</p>
        <div class="button btn">Закрыть окно</div>
    </div>
    <script async src="//app.konget.ru/inject?token=d9ae2e39eb2c4c7a86352789faf92398"></script>
    <script defer src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
    <script type="text/javascript" src="static/js/main.js?10"></script>
    <script type="text/javascript" src="static/js/jquery.visible.min.js"></script>
    <script src="static/js/jquery.inputmask.min.js" type="text/javascript"></script>
    <script src="static/js/slick.min.js" type="text/javascript"></script>
    <script type="text/javascript" async src="https://app.comagic.ru/static/cs.min.js"></script>
</body>

</html>